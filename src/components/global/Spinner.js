import React from 'react';
import { View, TextInput, Text, StyleSheet, TouchableHighlight } from 'react-native';

import { Colors, Size, Fonts, Icons } from '@styles/index';


export const SpinnerInputNumber = props => {

    const { count, onChange, maxCount, minCount, disabled } = props;

    const onChangeInput = (value) => {
        if(isNaN(value)){
            return;
        }
        if(value >= minCount && value <= maxCount){
            onChange(value.toString());
        }
    }

    if(disabled){
        return (
            <View style={[styles.container, props.containerStyle]}>
                <View style={styles.btnContainerDisabled}>
                    <View 
                        style={{width: Size.scaleSize(23), hight: Size.scaleSize(23), backgroundColor: Colors.primary}}
                    />
                    
                </View>
                <View style={styles.textInputContainerDisabled}>
                    <Text allowFontScaling={false} style={styles.input}>{count}</Text> 
                </View>
                <View style={styles.btnContainerDisabled}>
                    <View 
                        style={{width: Size.scaleSize(23), hight: Size.scaleSize(23), backgroundColor: Colors.primary}}
                    />
                </View>
            </View>
        )
    }

    return (
        <View style={[styles.container, props.containerStyle]}>
            <TouchableHighlight 
                style={styles.btnContainerLeft}
                // onPress={() => {}}
                onPress={() => onChangeInput(parseInt(count) - 1)}
            >
                <View style={[styles.btnContainer, styles.btnContainerLeft]}>
                    <Icons.MinusSVG
                        size={Size.scaleSize(20)}
                        color={Colors.primary}
                    />
                </View>
            </TouchableHighlight>
            <View style={styles.textInputContainer}>
                <TextInput 
                    allowFontScaling={false}
                    keyboardType="number-pad"
                    style={styles.input}
                    value={count}
                    onChangeText={onChangeInput}
                />
            </View>
            <TouchableHighlight 
                style={styles.btnContainerRight}
                // onPress={() => {}}
                onPress={() => onChangeInput(parseInt(count) + 1)}
            >
                <View style={[styles.btnContainer, styles.btnContainerRight]}>
                    <Icons.PlusSVG
                        size={Size.scaleSize(20)}
                        color={Colors.primary}
                    />
                </View>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        height: 33,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    btnContainer: {
        backgroundColor: '#FFF',
        width: 30,
        height: 33,
        borderWidth: 1,
        borderColor: '#F2F2F2',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainerLeft: {
        borderBottomLeftRadius: 8,
        borderTopLeftRadius: 8
    },
    btnContainerRight: {
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8
    },
    btnContainerDisabled: {
        backgroundColor: '#888'
    },
    textInputContainer: {
        height: '100%',
        width: '35%',
        alignItems: 'center',
        borderColor: '#F2F2F2',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        backgroundColor: '#F2F2F2'
    },
    textInputContainerDisabled: {
        height: '100%',
        width: '35%',
        alignItems: 'center',
        borderColor: '#888',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        backgroundColor: "#F2F2F2"
    },
    input: {
        textAlign: 'center', 
        color: '#000', 
        fontFamily: Fonts.mulishRegular, 
        fontSize: Size.scaleFont(14),
        paddingHorizontal: Size.scaleSize(4), 
        paddingVertical: 0
    }

})