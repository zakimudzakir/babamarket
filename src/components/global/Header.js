import React from 'react';
import { View, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';

import { Images, Colors, Size, Icons } from '@styles/index';

export const Header = props => {

    const { navData, headerLeft, isSeller } = props;

    return (
        <ImageBackground
            source={Images.StatusBarHome}
            style={styles.container}
        >
                <View style={styles.headerLeft}>
                    {headerLeft}
                </View>
                <View style={styles.headerRight}>
                    <TouchableOpacity 
                        onPress={() => {}}
                        style={{marginRight: Size.scaleSize(5), padding: Size.scaleSize(5)}}
                    >
                        <Icons.BellSVG 
                            size={isSeller? 28 : 24}
                            color={'#FFF'}
                        />
                    </TouchableOpacity>
                    {isSeller? (
                        <TouchableOpacity
                            onPress={() => navData.navigation.navigate('ChatSeller')}
                            style={{padding: Size.scaleSize(5), marginLeft: 5}}
                        >
                            <Icons.ChatSVG 
                                size={22}
                                color={'#FFF'}
                            />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            onPress={() => navData.navigation.navigate('Cart')}
                            style={{padding: Size.scaleSize(5)}}
                        >
                            <Icons.CartSVG 
                                size={24}
                                color={'#FFF'}
                            />
                        </TouchableOpacity>
                    )}
                </View>
            </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.primary, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        height: 80
    },
    headerLeft: {
        marginLeft: Size.scaleSize(15), 
        marginVertical: Size.scaleSize(20)
    },
    headerRight: {
        marginRight: Size.scaleSize(15), 
        flexDirection: 'row', 
        alignItems: 'center'
    }
})