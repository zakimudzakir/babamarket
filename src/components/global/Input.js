import React from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Fonts from '@styles/Fonts';
import { Size, Icons } from '@styles/index';

export const InputText = props => {

    const { label, leftIcon, rightIcon, isPassword, containerStyle, containerInputStyle } = props;



    return (
        <View style={[styles.container, containerStyle]}>
            {label && <Text style={styles.labelInputText} allowFontScaling={false}>{label}</Text>}
            <View style={[styles.containerTextInput, containerInputStyle]}>
                {leftIcon}
                <TextInput 
                    ref={props.innerRef}
                    allowFontScaling={false}
                    placeholderTextColor="#BDBDBD"
                    autoCapitalize="none"
                    {...props}
                    secureTextEntry={props.secureTextEntry}
                    style={[styles.inputText, props.style]}
                />
                {isPassword? (
                    <TouchableOpacity onPress={() => props.setSecureTextEntry(!props.secureTextEntry)}>
                        <Icons.EyeLeashedSVG 
                            size={Size.scaleSize(20)}
                            color="#B4B6B8"
                            style={{marginLeft: Size.scaleSize(10)}}
                        />
                    </TouchableOpacity>
                ) : rightIcon}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        
    },
    labelInputText: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        color: '#828282',
        marginBottom: Size.scaleSize(4)
    },
    inputText: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        color: '#828282',
        flex: 1
    },
    containerTextInput: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        width: '100%',
        borderColor: '#E0E0E0',
        borderRadius: 10,
        paddingHorizontal: 15
    }
})