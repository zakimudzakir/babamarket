import React, { useState, Component } from 'react';
import { 
    TouchableHighlight, 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    ViewPropTypes as RNViewPropTypes 
} from 'react-native';
import PropTypes from 'prop-types';

import { Size, Fonts, Colors, Icons, Images } from '@styles/index';

export const Button = props => {

    const { title, onPress, underlayColor, containerStyle, style, textStyle, pressColor, textComponent, disabled } = props;

    const [btnColor, setBtnColor] = useState(textStyle? textStyle.color : styles.buttonText.color);

    return (
        <View style={[styles.containerStyle, containerStyle]}>
            <TouchableHighlight 
                disabled={disabled}
                onPressIn={() => {pressColor? setBtnColor(pressColor) : null}}
                onPressOut={() => {pressColor? setBtnColor(textStyle? textStyle.color : styles.buttonText.color) : null}}
                style={[styles.button, style]}
                underlayColor={pressColor? btnColor : (underlayColor? underlayColor : "rgba(0,0,0,0.2)")}
                onPress={onPress}
            >
                <View>
                    {textComponent? textComponent : (
                        <Text 
                            allowFontScaling={false} 
                            style={[styles.buttonText, textStyle, pressColor && { color: btnColor }]}
                        >{title}</Text>
                    )}
                </View>
            </TouchableHighlight>
        </View>
    )
}

export const ButtonContainer = props => {

    const { style, onPress, title, desc, image, styleItem, textStyle, underlayColor } = props;

    return (
        <TouchableHighlight 
            onPress={onPress}
            underlayColor={underlayColor? underlayColor : "#efefef"}
            style={style}
        >
            <View style={[styles.btnContainer, styleItem ]}>
                <View style={{flex: 1}}>
                    {image}
                </View>
                <View style={{flex: 5}}>
                    <Text allowFontScaling={false} style={[styles.btnTitleContainer, textStyle]}>{title}</Text>
                    {desc && <Text allowFontScaling={false} style={[styles.btnDescContainer, textStyle]}>{desc}</Text>}
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Icons.ChevronRight 
                        color={textStyle? textStyle.color : "#B4B6B8"}
                        size={Size.scaleSize(20)}
                    />
                </View>
            </View>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        // height: Size.scaleSize(54),
        backgroundColor: Colors.primary,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '100%', 
        // height: '100%',
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#fff',
        textAlign: 'center',
        fontSize: Size.scaleFont(16),
        paddingVertical: Size.scaleSize(14)
    },

    btnContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        paddingHorizontal: Size.scaleSize(10), 
        paddingVertical: Size.scaleSize(16)
    },
    btnTitleContainer: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#333333',
        fontSize: Size.scaleFont(14),
        marginBottom: Size.scaleSize(4)
    },
    btnDescContainer: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(12),
        color: '#B4B6B8'
    }
})

const ViewPropTypes = RNViewPropTypes || View.propTypes;

export class CheckBox extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        ...ViewPropTypes,
        leftText: PropTypes.string,
        leftTextView: PropTypes.element,
        rightText: PropTypes.string,
        leftTextStyle: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
            PropTypes.object,
        ]),
        rightTextView: PropTypes.element,
        rightTextStyle: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
            PropTypes.object,
        ]),
        checkedImage: PropTypes.element,
        unCheckedImage: PropTypes.element,
        onClick: PropTypes.func.isRequired,
        isChecked: PropTypes.bool.isRequired,
        isIndeterminate: PropTypes.bool.isRequired,
        checkBoxColor: PropTypes.string,
        checkedCheckBoxColor: PropTypes.string,
        uncheckedCheckBoxColor: PropTypes.string,
        disabled: PropTypes.bool,
    }
    static defaultProps = {
        isChecked: false,
        isIndeterminate: false,
        leftTextStyle: {},
        rightTextStyle: {}
    }

    onClick() {
        this.props.onClick();
    }

    _renderLeft() {
        if (this.props.leftTextView) return this.props.leftTextView;
        if (!this.props.leftText) return null;
        return (
            <Text allowFontScaling={false} style={[checkBoxStyles.leftText, this.props.leftTextStyle]}>{this.props.leftText}</Text>
        );
    }

    _renderRight() {
        if (this.props.rightTextView) return this.props.rightTextView;
        if (!this.props.rightText) return null;
        return (
            <Text allowFontScaling={false} style={[checkBoxStyles.rightText, this.props.rightTextStyle]}>{this.props.rightText}</Text>
        );
    }

    _renderImage() {
        if (this.props.isIndeterminate) {
            return this.props.indeterminateImage ? this.props.indeterminateImage : this.genCheckedImage();
        }
        if (this.props.isChecked) {
            return this.props.checkedImage ? this.props.checkedImage : this.genCheckedImage();
        } else {
            return this.props.unCheckedImage ? this.props.unCheckedImage : this.genCheckedImage();
        }
    }

    _getCheckedCheckBoxColor() {
        return this.props.checkedCheckBoxColor ? this.props.checkedCheckBoxColor : this.props.checkBoxColor
    }

    _getUncheckedCheckBoxColor() {
        return this.props.uncheckedCheckBoxColor ? this.props.uncheckedCheckBoxColor : this.props.checkBoxColor
    }

    _getTintColor() {
        return this.props.isChecked ? this._getCheckedCheckBoxColor() : this._getUncheckedCheckBoxColor()
    }

    genCheckedImage() {
        let source;
        if (this.props.isIndeterminate) {
            source = Images.ICIndeterminatedCheckBox;
        }
        else {
            source = this.props.isChecked ? Images.ICCheckBox : Images.ICCheckBoxOutlineBlank;
        }

        return (
            <Image source={source} style={{tintColor: this._getTintColor()}}/>
        );
    }

    render() {
        return (
            <TouchableHighlight
                style={this.props.style}
                onPress={() => this.onClick()}
                underlayColor='transparent'
                disabled={this.props.disabled}
            >
                <View style={checkBoxStyles.container}>
                    {this._renderLeft()}
                    {this._renderImage()}
                    {this._renderRight()}
                </View>
            </TouchableHighlight>
        );
    }
}
const checkBoxStyles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    leftText: {
        flex: 1,
    },
    rightText: {
        flex: 1,
        marginLeft: 10
    }
});