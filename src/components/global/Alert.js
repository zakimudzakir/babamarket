import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

import { Button } from './Button';
import { Fonts, Icons, Colors } from '@styles/index';


const { width } = Dimensions.get('screen');

export const Alert = ({ show, modalClosed, title, message }) => {

    return (
        <Modal
            backdropColor="#aaa" 
            isVisible={show} 
            animationIn="fadeInDown"
            animationOut="fadeOutUp"
            onBackdropPress={modalClosed}
            useNativeDriver={true}
            hideModalContentWhileAnimating={true}
            onBackButtonPress={modalClosed}
        >
            <View style={styles.container}>
                <View style={styles.closeContainer}>
                    <TouchableOpacity
                        onPress={modalClosed}
                    >
                        <View style={styles.btnCloseContainer}>
                            
                            {/* <AntDesign name="close" size={24} color="black" /> */}
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.title}>{title? title : "ERROR"}</Text>
                    <View style={styles.messageContainer}>
                        <Text style={styles.message}>{message? message : 'Pesan Error'}</Text>
                    </View>
                    <Button 
                        title="OK"
                        containerStyle={{width: '50%', backgroundColor: '#F1AD5B'}}
                        
                        textStyle={{paddingVertical: 10, color: '#FFF'}}
                        onPress={modalClosed}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width * 0.9,
        paddingVertical: 10,
        backgroundColor: Colors.primaryDark
        
    },
    closeContainer: {
        alignSelf: 'flex-end'
    },
    btnCloseContainer: {
        padding: 2
    },
    contentContainer: {
        paddingBottom: 10,
        marginHorizontal: 10,
        alignItems: 'center'
    },
    title: {
        fontFamily: Fonts.mulishBold,
        fontSize: 18,
        color: '#FFF',
        textAlign: 'center',
        marginBottom: 40
    },
    messageContainer: {
        marginBottom: 30
    },
    message: {
        fontFamily: Fonts.mulishRegular,
        textAlign: 'center',
        color: '#FFF'
    }
})