import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

import { Fonts, Size } from '@styles/index';

const Store = props => {

    const { image, label, onPress, style } = props;

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <>
                    <View style={styles.containerImage}>
                        <Image 
                            style={[styles.image, style]}
                            source={image}
                        />
                    </View>
                    <Text allowFontScaling={false} style={styles.text}>{label}</Text>
                </>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '30%',
        alignItems: 'center',
        marginTop: Size.scaleSize(20)
    },
    containerImage: {
        width: Size.scaleSize(Dimensions.get('screen').width * 0.25),
        height: Size.scaleSize(Dimensions.get('screen').width * 0.25),
        borderRadius: Size.scaleSize(100),
        borderWidth: 4,
        borderColor: '#F2F4F5',
        alignItems: 'center',
        justifyContent: 'center'

    },
    image: {
        width: Size.scaleSize(Dimensions.get('screen').width * 0.18),
        height: Size.scaleSize(Dimensions.get('screen').width * 0.18),
        borderRadius: Size.scaleSize(Dimensions.get('screen').width * 0.18)
    },
    text: {
        fontFamily: Fonts.mulishMedium,
        color: '#333',
        fontSize: Size.scaleFont(14),
        textAlign: 'center'
    }
})

export default Store;