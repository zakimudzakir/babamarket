import React from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight, Dimensions } from 'react-native';
import { Size, Icons, Fonts, Colors, Images } from '@styles/index';
import { rupiahFormat } from '@helpers/functionFormat';

const Product = props => {

    const { containerStyle, onPress, data } = props;

    return (
        <TouchableHighlight 
            style={[styles.container, containerStyle]} 
            onPress={onPress}
            underlayColor={"#cdcdcd"}
            // underlayColor={Colors.opacityColor(Colors.primary, .5)}
        >
            <View>
                {data ? (
                <>
                <Image 
                    style={styles.image}
                    source={{uri: data.photo}}
                />
                <View style={styles.locationContainer}>
                    <Icons.LocationSVG 
                        size={16}
                        color={"#B4B6B8"}
                    />
                    <Text allowFontScaling={false} style={styles.location}>{data.location}</Text>
                </View>
                <Text allowFontScaling={false} style={styles.name}>{data.name}</Text>
                <Text allowFontScaling={false} style={styles.category}>{data.type}</Text>
                <Text allowFontScaling={false} style={styles.price}>{rupiahFormat(data.price)}</Text>
                </>
                ) : (
                    <>
                
                <Image 
                    style={styles.image}
                    source={Images.Product}
                />
                <View style={styles.locationContainer}>
                    <Icons.LocationSVG 
                        size={16}
                        color={"#B4B6B8"}
                    />
                    <Text allowFontScaling={false} style={styles.location}>Yogyakarta</Text>
                </View>
                <Text allowFontScaling={false} style={styles.name}>Semen Gresik 40-50 kg</Text>
                <Text allowFontScaling={false} style={styles.category}>Material Bangunan</Text>
                <Text allowFontScaling={false} style={styles.price}>Rp 57.000</Text>
                </>
                
                )}
            </View>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderColor: '#F2F4F5',
        width: Size.scaleSize(Dimensions.get('screen').width * 0.4),
        // alignItems: 'center'
        padding: Size.scaleSize(12),
        marginLeft: Size.scaleSize(15),
        borderRadius: 8
    },
    image: {
        width: '100%', 
        height: Size.scaleSize(132), 
        backgroundColor: '#FFF', 
        borderRadius: 8,
        alignSelf: 'center'
    },
    locationContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: Size.scaleSize(5)
    },
    location: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(12),
        marginLeft: Size.scaleFont(5)
    },
    name: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#333',
        marginTop: Size.scaleSize(5),
        fontSize: Size.scaleFont(14)
    },
    category: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(12),
        marginTop: Size.scaleSize(5),
    },
    price: {
        fontFamily: Fonts.mulishExtraBold,
        color: Colors.primary,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(5),
    }
})

export default Product;