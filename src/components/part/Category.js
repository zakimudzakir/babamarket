import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';

import { Size, Fonts } from '@styles/index';

const Category = props => {

    const { label, onPress, icon, style } = props;

    return (
        <TouchableOpacity onPress={onPress} style={styles.container}>
            <View>
                <View style={styles.iconContainer}>
                    {icon? icon : (
                        <View style={styles.icon}/>
                    )}
                </View>
                <Text allowFontScaling={false} style={styles.text}>{label}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '24%',
        alignItems: 'center'
    },
    iconContainer: {
        width: Size.scaleSize(69), 
        height:  Size.scaleSize(69), 
        backgroundColor: '#FFF2F3', 
        borderRadius: 100, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    icon: {
        width:  Size.scaleSize(41), 
        height:  Size.scaleSize(41), 
        borderRadius: 80, 
        backgroundColor: '#C4C4C4'
    },
    text: {
        textAlign: 'center', 
        marginTop: 5, 
        fontFamily: Fonts.mulishRegular, 
        color: '#333333',
        fontSize: Size.scaleFont(14)
    },

})

export default Category;