import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Icons, Size, Fonts, Colors } from '@styles/index';
import { Button } from '@components/global';

const OrderItem = props => {

    const changeColorStatus = (type) => {
        switch(type){
            case 1: {
                return { color: '#FF5247' }
            }
            case 2: {
                return { color: '#FFB323' }
            }
            case 3: {
                return { color: '#1DA1F2' }
            }
            case 4: {
                return { color: '#23C16B' }
            }
            default: {
                return { color: '#333333' }
            }
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.idContainer}>
                <Text allowFontScaling={false} style={styles.idText}>{props.id}</Text>
                <Text 
                    allowFontScaling={false} 
                    style={[
                        styles.statusText,
                        changeColorStatus(props.statusType)
                    ]}
                >
                    {props.status}
                </Text>
            </View>
            <View style={styles.priceContainer}>
                <View style={styles.dateContainer}>
                    <Text allowFontScaling={false} style={styles.dateText}>{props.date}</Text>
                    <View style={styles.ballDate}/>
                    <Text allowFontScaling={false} style={styles.dateText}>{props.time}</Text>
                </View>
                <Text 
                    allowFontScaling={false} 
                    style={styles.totalText}
                >
                    Total: <Text allowFontScaling={false} style={styles.priceText}>{props.price}</Text>
                </Text>
            </View>
            <View style={styles.btnContainer}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icons.ShoppingBagSVG
                        size={Size.scaleSize(18)} 
                        color={'#333'}
                        style={{marginRight: Size.scaleSize(5)}}
                    />
                    <Text allowFontScaling={false} style={styles.countItem}>{props.count} item dibeli</Text>
                </View>
                <Button
                    style={{paddingHorizontal: Size.scaleSize(25), paddingVertical: 0}}
                    textStyle={{paddingVertical: Size.scaleSize(8), fontSize: Size.scaleFont(12)}}
                    title="Lihat Detail"
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: Size.scaleSize(20), 
        borderColor: '#F2F4F5', 
        borderWidth: 1, 
        marginVertical: Size.scaleSize(10), 
        borderRadius: 16
    },
    idContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
        marginBottom: Size.scaleSize(5)
    },
    idText: {
        fontFamily: Fonts.mulishBold,
        fontSize: Size.scaleFont(14),
        color: '#333'
    },
    statusText: {
        fontFamily: Fonts.mulishBold,
        fontSize: Size.scaleFont(14),
    },
    priceContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginBottom: Size.scaleSize(12)
    },
    dateContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ballDate: {
        width: Size.scaleSize(4),
        height: Size.scaleSize(4),
        borderRadius: Size.scaleSize(10),
        marginHorizontal: Size.scaleSize(5),
        backgroundColor: '#828282'
    },
    dateText: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(12),
        color: '#828282'
    },
    priceText: {
        fontFamily: Fonts.mulishBold,
        fontSize: Size.scaleSize(14),
        color: '#333'
    },
    totalText: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleSize(14),
        color: '#333'
    },
    btnContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        paddingTop: Size.scaleSize(18), 
        borderTopColor: '#F7F9FA', 
        borderTopWidth: 1
    },
    countItem: {
        fontFamily: Fonts.mulishRegular, 
        color: '#333', 
        fontSize: Size.scaleFont(14)
    }
})

export default OrderItem;