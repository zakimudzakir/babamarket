import { AUTHENTICATE, LOGOUT, SET_USER_DATA, SET_USER_LOCATION, SET_USER_LOCATION_ADDRESS } from "@store/actions/auth";

const initialState = {
    token: null,
    tokenType: null,
    userData: {
        role: null,
        name: null,
        phone: null,
        email: null
    },
    location: {
        latitude: null,
        longitude: null,
        address: null,
        city: null
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case AUTHENTICATE: {
            return {
                ...state,
                token: action.token,
                tokenType: action.tokenType
            }
        }
        case SET_USER_DATA: {
            return {
                ...state,
                userData: {
                    ...state.userData,
                    role: action.role,
                    name: action.name,
                    phone: action.phone,
                    email: action.email
                }
            }
        }
        case SET_USER_LOCATION: {
            return {
                ...state,
                location: {
                    ...state.location,
                    latitude: action.latitude,
                    longitude: action.longitude
                }
            }
        }
        case SET_USER_LOCATION_ADDRESS: {
            return {
                ...state,
                location: {
                    ...state.location,
                    city: action.city
                }
            }
        }
        case LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }
}