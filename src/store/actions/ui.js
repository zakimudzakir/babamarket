export const UPDATE_LOADING = 'UPDATE_LOADING';
export const UPDATE_MODAL = 'UPDATE_MODAL';
export const SHOW_MODAL = 'SHOW_MODAL';

export const updateLoading = (loading) => {
    return {
        type: UPDATE_LOADING,
        loading: loading
    }
}

export const updateModal = (show, title, message) => {
    return {
        type: UPDATE_MODAL,
        modal: {
            show: show,
            title: title,
            message: message
        }
    }
}

export const showModal = (show) => {
    return {
        type: SHOW_MODAL,
        show: show
    }
}