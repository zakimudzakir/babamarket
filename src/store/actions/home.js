import { API } from "@constants/url";

export const getHomeBuyer = () => {
    return async (dispatch, getState) => {

        const tokenType = getState().auth.tokenType;
        const token = getState().auth.token;

        const response = await fetch(API + '/home-buyer', {
            method: 'GET',
            headers: {
                "Accept": "application/json",
                Authorization: `${tokenType} ${token}`
            }
        })


        const resData = await response.json();

        if(!response.ok){
            throw new Error(resData.data.info);
        }

        if(!resData.success){
            throw new Error(resData.data.info);
        }

        return resData.data;
    }
}