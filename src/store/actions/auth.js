import { API } from "@constants/url";
import { updateLoading } from "./ui";

export const AUTHENTICATE = 'AUTHENTICATE';
export const SET_USER_DATA = 'SET_USER_DATA';
export const SET_USER_LOCATION = 'SET_USER_LOCATION';
export const SET_USER_LOCATION_ADDRESS = 'SET_USER_LOCATION_ADDRESS';
export const LOGOUT = 'LOGOUT';

const authenticate = (token, tokenType) => {
    return {
        type: AUTHENTICATE,
        token: token,
        tokenType: tokenType
    }
}

const setUserData = (role, name, phone, email) => {
    return {
        type: SET_USER_DATA,
        role: role,
        name: name,
        phone: phone,
        email: email,
    }
}

export const setUserLocation = (latitude, longitude) => {
    return async (dispatch) => {
        await dispatch(updateLoading(true));

        await dispatch({
            type: SET_USER_LOCATION,
            latitude: latitude,
            longitude: longitude
        })

        await dispatch(setUserAddressLocation())
        await dispatch(updateLoading(false));
    }
}

export const setUserAddressLocation = () => {
    return async (dispatch, getState) => {
        await dispatch(updateLoading(true));

        const latitude = getState().auth.location.latitude;
        const longitude = getState().auth.location.longitude;

        const response = await fetch(`https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${latitude}&longitude=${longitude}&localityLanguage=id`, {
            method: 'GET',
            headers: {
                "Accept": "application/json"
            }
        })

        const resData = await response.json();

        if(!response.ok){
            await dispatch(updateLoading(false));
            throw new Error(resData.message);
        }

        await dispatch({
            type: SET_USER_LOCATION_ADDRESS,
            city: resData.principalSubdivision
        })

    }
}

export const login = (email, password) => {
    return async (dispatch, getState) => {
        await dispatch(updateLoading(true));

        const formData = new FormData();

        formData.append("email", email);
        formData.append("password", password);

        const response = await fetch(API + '/login', {
            method: 'POST',
            headers: {
                "Content-Type": "multipart/form-data",
                "Accept": "application/json"
            },
            body: formData
        })


        const resData = await response.json();
        

        if(!response.ok){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }

        if(!resData.success){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }

        await dispatch(authenticate(resData.data.token, resData.data.token_type));

        await dispatch(getProfile());
        await dispatch(updateLoading(false));

    }
}

export const register = (name, phone, email, password) => {
    return async (dispatch, getState) => {
        await dispatch(updateLoading(true));

        const formData = new FormData();

        formData.append("name", name);
        formData.append("phone", phone);
        formData.append("email", email);
        formData.append("password", password);

        const response = await fetch(API + '/register', {
            method: 'POST',
            headers: {
                "Content-Type": "multipart/form-data",
                "Accept": "application/json"
            },
            body: formData
        })


        const resData = await response.json();

        if(!response.ok){
            await dispatch(updateLoading(false));
            throw new Error(resData.message);
        }

        if(!resData.success){
            await dispatch(updateLoading(false));
            throw new Error(resData.message);
        }

        await dispatch(authenticate(resData.data.token, resData.data.token_type));
        await dispatch(setUserData(resData.data.role, resData.data.name, resData.data.phone, resData.data.email));

        await dispatch(updateLoading(false));
    }
}

export const getProfile = () => {
    return async (dispatch, getState) => {
        await dispatch(updateLoading(true));

        const tokenType = getState().auth.tokenType;
        const token = getState().auth.token;

        const response = await fetch(API + '/profile', {
            method: 'GET',
            headers: {
                "Accept": "application/json",
                Authorization: `${tokenType} ${token}`
            }
        })

        const resData = await response.json();
        console.log(resData);

        if(!response.ok){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }

        if(!resData.success){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }

        await dispatch(setUserData(resData.data.is_seller === "no"? "buyer" : "seller", resData.data.name, resData.data.phone, resData.data.email));
        await dispatch(updateLoading(false));
    }
}

export const logout = () => {
    return {
        type: LOGOUT
    }
}