import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';

import thunk from 'redux-thunk';
import rootReducer from './reducers';

const persistConfig = {
	// Root
	key: 'baba-market-str',
	// Storage Method (React Native)
	storage: AsyncStorage,
	timeout: null,
	// Whitelist (Save Specific Reducers)
	whitelist: ['lang', 'auth'],
	// blacklist: ['homeReducer']
};
// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);
// Redux: Store
const store = createStore(
	persistedReducer,
	applyMiddleware(
		thunk,
		// createLogger(),
	),
);
// Middleware: Redux Persist Persister
let persistor = persistStore(store);
// Exports
export { store, persistor };