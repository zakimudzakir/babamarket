import { check, request, PERMISSIONS, RESULTS, requestMultiple } from 'react-native-permissions';
import { Platform } from 'react-native';

const REQUEST_PERMISSION_TYPE = {
    camera: {
        ios: PERMISSIONS.IOS.CAMERA,
        android: PERMISSIONS.ANDROID.CAMERA
    },
    photo: {
        ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
        android: PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
    },
    location: {
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
    }
}

export const PERMISSIONS_TYPE = {
    camera: 'camera',
    photo: 'photo',
    location: 'location'
}

export const checkPermissions = async (type) => {
    const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS];
    if(!permissions){
        return true;
    }
    try{
        const result = await check(permissions);
        if(result === RESULTS.GRANTED) return true;
        return requestPermission(type);
    }catch(error){
        return false;
    }   
}

export const requestPermission = async (type) => {
    const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS];
    try{
        const result = await request(permissions);
        return result === RESULTS.GRANTED;
    }catch(error){
        return false;
    }
}