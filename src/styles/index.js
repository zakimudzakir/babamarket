import Colors from './Colors';
import Fonts from './Fonts';
import * as Size from './Size';
import * as Icons from './Icons';
import * as Images from './Images';

export { Fonts, Colors, Size, Icons, Images };