//logo
export const BabaMarket = require('@assets/images/BabaMarket.png');

//ui
export const StatusBarHome = require('@assets/images/HeaderTop.png');
export const HeaderHome = require('@assets/images/HeaderBottom.png');

//image
export const Banner = require('@assets/images/banner.png');
export const Product = require('@assets/images/Product.png');
export const Store = require('@assets/images/Store.png');
export const User = require('@assets/images/User.png');

//bank
export const BankBCA = require('@assets/images/logo-BCA.png');
export const BankMandiri = require('@assets/images/logo-Mandiri.png');

//checkbox
export const ICCheckBox = require('@assets/images/checkbox/ic_check_box.png');
export const ICIndeterminatedCheckBox = require('@assets/images/checkbox/ic_indeterminate_check_box.png');
export const ICCheckBoxOutlineBlank = require('@assets/images/checkbox/ic_check_box_outline_blank.png');