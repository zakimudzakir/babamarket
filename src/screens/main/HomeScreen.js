import React, { useEffect, useState } from 'react';
import { ScrollView, View, Text, FlatList, StyleSheet, ImageBackground, TouchableOpacity, Image, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { Colors, Fonts, Size, Icons, Images } from '@styles/index';
import { InputText, Button, Header } from '@components/global';
import Category from '@components/part/Category';
import Product from '@components/part/Product';
import Store from '@components/part/Store';
import { getHomeBuyer } from '@store/actions';

const HomeScreen = props => {
    // const products = [{id: '1'}, {id: '2'}, {id: '3'}];
    const [categories, setCategories] = useState([]);
    const [products, setProducts] = useState([]);
    const auth = useSelector(state => state.auth);

    const dispatch = useDispatch();

    useEffect(() => {
        loadData();
    }, [])

    const loadData = async () => {
        try {
            const resData = await dispatch(getHomeBuyer());
            setCategories(resData.categories);
            setProducts(resData.products);
        } catch(error) {
            console.log(error);
        }
    }

    return (
        <ScrollView>
            <View style={styles.screen}>
                <ImageBackground
                    source={Images.HeaderHome}
                    style={{backgroundColor: Colors.primary, height: 170}}
                >
                    <View style={{flexDirection: 'row', alignItems: 'center', marginHorizontal: Size.scaleSize(15), marginBottom: Size.scaleSize(20)}}>
                        <Icons.LocationSVG 
                            size={24}
                            color="white"
                        />
                        <View style={{marginLeft: Size.scaleSize(10)}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: 'white', fontSize: Size.scaleFont(14)}}>Lokasi Saya</Text>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishMedium, color: 'white', fontSize: Size.scaleFont(14)}}>{auth.location.city}</Text>
                        </View>
                    </View>
                    <InputText 
                        containerStyle={{marginHorizontal: Size.scaleSize(18)}}
                        containerInputStyle={{backgroundColor: 'white'}}
                        placeholder="Cari nama produk"
                        rightIcon={<Icons.SearchSVG 
                            size={24}
                            color="black"
                        />}
                    />
                </ImageBackground>
                <View style={{top: Size.scaleSize(-35), backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40 }}>
                    <Image 
                        style={{width: Dimensions.get('screen').width * 0.9, height: Dimensions.get('screen').width * 0.6, alignSelf: 'center', }}
                        resizeMode="contain"
                        source={Images.Banner}
                    />
                    <Text allowFontScaling={false} style={styles.titleText}>Kategori</Text>
                    <View style={{marginHorizontal: 20, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
                        {categories.map(category => (
                            <Category
                                key={category.id.toString()}
                                style={{width: '24%'}}
                                label={category.name}
                                onPress={() => props.navigation.navigate('Category')}
                            />
                        ))}
                        
                        {/* <Category
                            style={{width: '24%'}}
                            label={"Alat\nTukang"}
                            onPress={() => props.navigation.navigate('Category')}
                        />
                        <Category
                            style={{width: '24%'}}
                            label={"Peralatan\nFurniture"}
                            onPress={() => props.navigation.navigate('Category')}
                        />
                        <Category
                            style={{width: '24%'}}
                            label={"Kategori\nLainnya"}
                            icon={<Icons.CategorySVG 
                                size={Size.scaleSize(41)}
                                color={Colors.primary}
                            />}
                            onPress={() => props.navigation.navigate('Category')}
                        /> */}
                    </View>
                    <View style={styles.titleProductContainer}>
                        <Text allowFontScaling={false} style={styles.titleProductText}>Produk Terlaris</Text>
                        <TouchableOpacity onPress={() => {}}>
                            <View style={styles.seeAll}>
                                <Text allowFontScaling={false} style={styles.seeAllText}>Lihat Semua</Text>
                                <Icons.ArrowRightSVG 
                                    size={24}
                                    color={Colors.primary}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <FlatList 
                        style={{marginVertical: Size.scaleSize(20) }}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={products}
                        keyExtractor={item => item.id}
                        renderItem={(itemData) => (
                            <Product 
                                containerStyle={itemData.index === products.length - 1 && {marginRight: Size.scaleSize(15)}}
                                onPress={() => props.navigation.navigate('Product', { product: itemData.item })}
                                data={itemData.item}
                            />
                            // <Product 
                            //     containerStyle={itemData.index === products.length - 1 && {marginRight: Size.scaleSize(15)}}
                            //     onPress={() => props.navigation.navigate('Product')}
                            // />
                        )}
                    />
                    <Text allowFontScaling={false} style={styles.titleText}>Toko Disekitarmu</Text>
                    <View style={{flexDirection: 'row', marginHorizontal: 20, justifyContent: 'space-between', flexWrap: 'wrap', marginBottom: Size.scaleSize(30)}}>
                        <Store 
                            image={Images.Store}
                            label="UD Jaya Makmur"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                        <Store 
                            image={Images.Store}
                            label="Sejahtera Bangun"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                        <Store 
                            image={Images.Store}
                            label="Abadi Jaya"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                        <Store 
                            image={Images.Store}
                            label="Homecare"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                        <Store 
                            image={Images.Store}
                            label="Toko Sanowo"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                        <Store 
                            image={Images.Store}
                            label="Kokoh Abadi"
                            onPress={() => props.navigation.navigate('Store')}
                        />
                    </View>
                    <Button 
                        containerStyle={{top: 20, marginHorizontal: 20, backgroundColor: '#FFF'}}
                        style={{borderWidth: 2, borderColor: Colors.primary}}
                        textStyle={{color: Colors.primary}}
                        title="Lihat Semua"
                        pressColor={"#FFF"}
                        onPress={() => {}}
                    />
                </View>
            </View>
        </ScrollView>
    )
}

export const screenOptions = navData => {
    const name = useSelector(state => state.auth.userData.name);
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14)}}>Selamat Datang,</Text>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>{name}</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    titleText: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333', 
        marginHorizontal: Size.scaleSize(20), 
        fontSize: Size.scaleFont(20)
    },
    titleProductContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginHorizontal: Size.scaleSize(20), 
        marginTop: Size.scaleSize(40)
    },
    titleProductText: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333', 
        fontSize: Size.scaleFont(20)
    },
    seeAll: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    seeAllText: {
        fontFamily: Fonts.mulishRegular, 
        color: Colors.primary, 
        fontSize: Size.scaleFont(14), 
        marginRight: Size.scaleSize(5)
    }
})

export default HomeScreen;