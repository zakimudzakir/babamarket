import React from 'react';
import { View, Text, ScrollView, StyleSheet, Image,  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { Header, Card, ButtonContainer } from '@components/global';
import { Size, Fonts, Images, Icons, Colors } from '@styles/index';
import { logout } from '@store/actions';

const UserScreen = props => {

    const dispatch = useDispatch();

    const user = useSelector(state => state.auth.userData);

    const onLogout = async () => {
        try{
            await dispatch(logout());
            props.navigation.replace("AuthNavigation")
        }catch(error){

        }
    }

    return (
        <View style={styles.screen}>
            <ScrollView>
                <View style={{flexDirection: 'row', alignItems: 'center', margin: Size.scaleSize(20)}}>
                    <Image 
                        source={Images.User}
                        style={styles.image}
                    />
                    <View style={{flex: 1, marginLeft: 20}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(18), marginBottom: Size.scaleSize(4)}}>{user.name}</Text>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', fontSize: Size.scaleFont(12)}}>{user.email}</Text>
                    </View>
                </View>
                <ButtonContainer 
                    style={{borderRadius: 10, paddingHorizontal: 20, backgroundColor: Colors.primary, marginHorizontal: 18}}
                    textStyle={{color: '#FFF'}}
                    underlayColor={Colors.opacityColor(Colors.primary, .8)}
                    image={(
                        <Icons.StoreSVG 
                            color={"#FFF"}
                            size={Size.scaleSize(24)}
                        />
                    )}
                    title={"Toko Saya"}
                    desc={"Buat toko, kelola, jual produk"}
                    onPress={() => props.navigation.replace("AuthSellerNavigation")}
                            
                />
                <View style={{marginHorizontal: 18, paddingBottom: 10, marginTop: 10}}>
                    <Card style={{width: '100%', backgroundColor: 'white', borderRadius: 10, marginTop: 10}}>
                        <ButtonContainer 
                            style={{borderTopRightRadius: 10, borderTopLeftRadius: 10, paddingHorizontal: 20}}
                            image={(
                                <Icons.KidSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Profil"}
                            desc={"Lihat informasi profil kamu"}
                            onPress={() => props.navigation.navigate("Profile")}
                        />
                        <ButtonContainer 
                            style={{paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.LocationSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Alamat Saya"}
                            desc={"Pengaturan alamat/lokasi Anda"}
                            onPress={() => props.navigation.navigate("Address")}
                        />
                        <ButtonContainer 
                            style={{paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.CommentInfoSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Tentang Kami"}
                            desc={"Informasi mengenai Baba Market"}
                            // onPress={() => props.navigation.navigate("Currency")}
                        />
                        <ButtonContainer 
                            style={{paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.OutgoingCallSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Hubungi Kami"}
                            desc={"Hubungi admin kami untuk bantuan"}
                            // onPress={() => props.navigation.navigate("Currency")}
                        />
                        <ButtonContainer 
                            style={{paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.ClipboardNotesSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Syarat & Ketentuan"}
                            desc={"Pelajari syarat & ketentuan kami"}
                            // onPress={() => props.navigation.navigate("Currency")}
                        />
                        <ButtonContainer 
                            style={{borderBottomRightRadius: 10, borderBottomLeftRadius: 10, paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.ExitSVG 
                                    color={"#FF5247"}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Keluar"}
                            desc={"Keluar dari akun Anda"}
                            onPress={onLogout}
                        />
                    </Card>
                </View>
            </ScrollView>
        </View>
    )
}

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Akun Saya</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    image: {
        width: Size.scaleSize(60), 
        height: Size.scaleSize(60), 
        borderRadius: Size.scaleSize(60)
    }
})

export default UserScreen;