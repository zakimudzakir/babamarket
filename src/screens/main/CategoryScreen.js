import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StatusBar, FlatList, TouchableOpacity, StyleSheet, Animated, Dimensions, TouchableHighlight } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';

import { Size, Icons, Fonts, Images, Colors } from '@styles/index';
import Store from '@components/part/Store';
import Product from '@components/part/Product';
// import { SwipeGesture } from '@components/global';

const CategoryScreen = props => {

    const [isSwiped, setSwiped] = useState(false);
    const [btnContainerY] = useState(new Animated.Value(Dimensions.get('screen').height * 0.27));
    const [headerHeight] = useState(new Animated.Value(Dimensions.get('screen').height * 0.25));
    const [headerBottom] = useState(new Animated.Value(Size.scaleSize(100)))
    const [headerOpacity] = useState(new Animated.Value(1));

    const flatListRef = useRef();

    const [offsetScroll, setOffsetScroll] = useState(0);
    
    useEffect(() => {
        props.navigation.setParams({isSwiped: isSwiped})
    }, [isSwiped])

    const flatListScroll = (event) => {
        setOffsetScroll(event.nativeEvent.contentOffset.y);
        if(event.nativeEvent.contentOffset.y <= 10){
            downAnimation();
        }
        
    }

    const handleRelease = () => {
        flatlistRef.current.scrollToOffset({ y: -100 });
        setTimeout(() => {
            flatlistRef.current.scrollToOffset({ y: 0 });
        }, 1000)
    }
    

    const upAnimation = () => {
        Animated.timing(headerOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: false,
        }).start(() => {
            setSwiped(true);
            Animated.parallel([
                Animated.sequence([
                    Animated.timing(headerBottom, {
                        toValue: Size.scaleSize(0),
                        duration: 100,
                        useNativeDriver: false
                    }),
                    Animated.timing(headerHeight, {
                        toValue: 0,
                        duration: 100,
                        useNativeDriver: false
                    })
                ]),
                Animated.sequence([
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height * 0.27 - 20,
                        duration: 80,
                        useNativeDriver: true
                    }),
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height - 230,
                        duration: 100,
                        useNativeDriver: true
                    })
                ])
            ]).start(() => {
                setSwiped(true);
            })
            
            
        });
        
    }

    const downAnimation = () => {
        Animated.timing(headerBottom, {
            toValue: Size.scaleSize(100),
            duration: 100,
            useNativeDriver: false
        }).start(() => {
            Animated.timing(headerHeight, {
                toValue: Dimensions.get('screen').height * 0.25,
                duration: 100,
                useNativeDriver: false
            }).start(() => {
                setSwiped(false);
                if(offsetScroll > 0){
                    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
                }
                Animated.timing(headerOpacity, {
                    toValue: 1,
                    duration: 10,
                    useNativeDriver: false,
                }).start(() => {
                    setSwiped(false);
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height * 0.27,
                        duration: 120,
                        useNativeDriver: true
                    }).start();
                });
            });
            
        })
    }
 
    return (
        <GestureRecognizer
            style={styles.screen}
            config={{
                velocityThreshold: 0.2,
                // directionalOffsetThreshold: 80
            }}
            onSwipeUp={!isSwiped? () => upAnimation() : null}
            // onSwipeDown={() => console.log('test')}
            // onSwipeDown={() => {
            //     if(isSwiped && flatListRef.current && flatListRef.current <= 0){
            //         downAnimation();
            //     }
            // }}
        >
            <StatusBar 
                backgroundColor="#FFF"
                barStyle="dark-content"
            />
            
            <Animated.View style={[styles.headerCategory,{ height: headerHeight, marginBottom: headerBottom, opacity: headerOpacity}]}>
                {!isSwiped && <Store 
                    image={Images.Store}
                    label=""
                />}
                <Text 
                    allowFontScaling={false}
                    style={styles.titleCategory}
                >
                    Material Bangunan
                </Text>
                <Text 
                    allowFontScaling={false}
                    style={styles.infoTitleCategory}
                >
                    40 produk ditemukan
                </Text>
            </Animated.View>
            <Animated.View 
                style={[styles.btnContainer, {
                    transform: [{
                        translateY: btnContainerY
                    }]
                }]}
            >
                <TouchableHighlight 
                    style={{marginHorizontal: Size.scaleSize(5), borderRadius: 10, padding: Size.scaleSize(14), backgroundColor: 'white'}} 
                    onPress={() => {}}
                    underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
                >
                    <Icons.SortSVG 
                        size={Size.scaleSize(24)}
                        color={Colors.primary}
                    />
                </TouchableHighlight>
                <TouchableHighlight 
                    style={{marginHorizontal: Size.scaleSize(5), borderRadius: 10, padding: Size.scaleSize(14), backgroundColor: 'white'}} 
                    onPress={() => {}}
                    underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
                >
                    <Icons.FilterSVG 
                        size={Size.scaleSize(24)}
                        color={Colors.primary}
                    />
                </TouchableHighlight>
            </Animated.View>
            <View>
                <FlatList 
                    scrollEventThrottle={16}
                    contentContainerStyle={{paddingBottom: 10}}
                    ref={flatListRef}
                    scrollToOverflowEnabled={true}
                    scrollEnabled={isSwiped}
                    onScroll={flatListScroll}
                    // onScroll={Animated.event(
                    //     [{
                    //         nativeEvent: {
                    //             contentOffset: {
                    //                 y: scrolling
                    //             }
                    //         }
                    //     }],
                    //     { useNativeDriver : true }
                    // )}
                    ListHeaderComponent={<Text allowFontScaling={false} style={styles.title}>Rekomendasi</Text>}
                    // onMoveShouldSetResponder={() => true}
                    data={[{id: '0'}, {id: '1'}, {id: '2'}, {id: '3'}, {id: '4'}, {id: '5'}, {id: '6'}, {id: '7'}]}
                    columnWrapperStyle={{marginTop: 10, marginHorizontal: 0}}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    renderItem={(itemData) => (
                        <Product 
                            onPress={() => props.navigation.navigate('Product')}
                        />
                    )}
                />
            </View>
        </GestureRecognizer>
        
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerCategory: {
        alignItems: 'center', 
        justifyContent: 'center',
        height: Size.scaleSize(Dimensions.get('screen').height * 0.25),
        marginBottom: Size.scaleSize(100),
        opacity: 1
    },
    titleCategory: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333', 
        fontSize: Size.scaleFont(24)
    },
    infoTitleCategory: {
        fontFamily: Fonts.mulishRegular, 
        color: '#B4B6B8', 
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(5),
        
    },
    title: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333333', 
        fontSize: Size.scaleFont(16),
        marginHorizontal: Size.scaleSize(15)
    },
    btnContainer: { 
        position: 'absolute', 
        alignSelf: 'center', 
        backgroundColor: '#F7F9F8', 
        // top: Dimensions.get('screen').height * 0.27, 
        zIndex: 2, 
        padding: Size.scaleSize(10), 
        borderRadius: 20, 
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export const screenOptions = navData => {
    const isSwiped = navData.route.params && navData.route.params.isSwiped? navData.route.params.isSwiped : false;
    return {
        headerTitle: "Material Bangunan",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333',
            opacity: isSwiped? 1 : 0
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default CategoryScreen;