import React, { useEffect } from 'react';
import { View, Text, ScrollView, Image, StyleSheet, StatusBar, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AndroidKeyboardAdjust from 'react-native-android-keyboard-adjust';
import { useSelector } from 'react-redux';

import { Images, Size, Icons, Fonts } from '@styles/index';
import { Button, InputText } from '@components/global';

const ProfileScreen = props => {

    const user = useSelector(state => state.auth.userData)
    
    useEffect(() => {
        AndroidKeyboardAdjust.setAdjustPan();
    }, [])

    return (
        <ScrollView contentContainerStyle={{flex: 1}}>
            <StatusBar barStyle="dark-content" backgroundColor="#FFF"/>
            <KeyboardAwareScrollView style={{backgroundColor:"#FFF"}} enableOnAndroid={true}>
            <View style={styles.screen}>
            <Image 
                source={Images.User}
                style={styles.image}
            />
            <Button 
                containerStyle={{borderRadius: 10, marginTop: 10}}
                style={{borderRadius: 10}}
                textComponent={(
                    <View style={{flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 24}}>
                        <Text style={{fontFamily: Fonts.mulishMedium, color: '#FFF', fontSize: Size.scaleFont(14)}}>Ganti Foto</Text>
                        <Icons.CameraSVG
                            size={20}
                            color="#FFF"
                            style={{marginLeft: 20}}
                        />
                    </View>
                )}
            />
            <InputText 
                containerStyle={{marginHorizontal: 20, marginTop: 30}}
                label="Nama Lengkap"
                value={user.name}
            />
            <InputText 
                containerStyle={{marginHorizontal: 20, marginTop: 30}}
                label="Email"
                value={user.email}
            />
            <InputText 
                containerStyle={{marginHorizontal: 20, marginTop: 30}}
                keyboardType="phone-pad"
                label="No. Telepon"
                value={user.phone}
            />
            <InputText 
                containerStyle={{marginHorizontal: 20, marginTop: 30}}
                secureTextEntry={true}
                label="Kata Sandi"
                value=""
            />
            </View>
            </KeyboardAwareScrollView>
            <Button 
                // disabled={true}
                containerStyle={{marginHorizontal: 20, marginBottom: 10}}
                title="Simpan Perubahan"
            />
        </ScrollView>
    )
}

export const screenOptions = navData => {
    return {
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        ),
        headerTitle: 'Profil',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(14)
        },
        headerTitleAlign: 'left'
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    image: {
        width: Size.scaleSize(60),
        height: Size.scaleSize(60),
        borderRadius: Size.scaleSize(60)
    }
})

export default ProfileScreen;