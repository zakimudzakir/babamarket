import React from 'react';
import { View, Text,Image, StatusBar, TouchableOpacity, FlatList, } from 'react-native';

import { CheckBox, SpinnerInputNumber, Button } from '@components/global';
import { Icons, Fonts, Size, Images } from '@styles/index';

const CartScreen = props => {

    const data = [
        {
            id: '1',
            image: Images.Store,
            store: 'UD Jaya Makmur',
            products: [{
                id: '1',
                image: Images.Product,
                name: 'Semen Gresik 40-50 kg',
                desc: 'Bahan Bangunan, 40 kg',
                price: 'Rp. 57.000',
                quantity: "1"
            }, {
                id: '2',
                image: Images.Product,
                name: 'Semen Gresik 40-50 kg',
                desc: 'Bahan Bangunan, 40 kg',
                price: 'Rp. 57.000',
                quantity: "1"
            }]
        },
        {
            id: '2',
            image: Images.Store,
            store: 'UD Jaya Makmur',
            products: [{
                id: '1',
                image: Images.Product,
                name: 'Semen Gresik 40-50 kg',
                desc: 'Bahan Bangunan, 40 kg',
                price: 'Rp. 57.000',
                quantity: "1"
            }, {
                id: '2',
                image: Images.Product,
                name: 'Semen Gresik 40-50 kg',
                desc: 'Bahan Bangunan, 40 kg',
                price: 'Rp. 57.000',
                quantity: "1"
            }]
        }
    ]

    return (
        <View>
            <StatusBar barStyle="dark-content" backgroundColor={"#FFF"}/>
            <FlatList 
                contentContainerStyle={{paddingBottom: 150}}
                ListHeaderComponent={(
                    <View style={{flexDirection: 'row', alignItems: 'center', margin: 20 }}>
                        <CheckBox
                            onClick={() => {}}
                            style={{marginRight: 20}}
                        />
                        <Text allowFontScaling={false} style={{fontSize: Size.scaleFont(14), fontFamily: Fonts.mulishBlack, color: '#333'}}>Pilih Semua</Text>
                    </View>
                )}
                data={data}
                keyExtractor={item => item.id}
                renderItem={(({item}) => (
                    <View style={{marginHorizontal: 20, backgroundColor: '#FFF', padding: 20, borderRadius: 16, marginBottom: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderColor: '#F2F4F5', paddingBottom: 18}}>
                            <CheckBox
                                onClick={() => {}}
                                style={{marginRight: 15}}
                            />
                            <Image 
                                source={item.image}
                                style={{width: 36, height: 36, marginRight: 10}}
                            />
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333'}}>{item.store}</Text>
                        </View>
                        {item.products.map(product => (
                            <View key={product.id} style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10}}>
                                 <CheckBox
                                    onClick={() => {}}
                                    style={{marginRight: 15}}
                                />
                                <Image 
                                    source={product.image}
                                    style={{width: 60, height: 60, marginRight: 15}}
                                />
                                <View>
                                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333', marginBottom: 5}}>{product.name}</Text>
                                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8', marginBottom: 5}}>{product.desc}</Text>
                                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#333'}}>{product.price}</Text>
                                    <SpinnerInputNumber
                                        containerStyle={{justifyContent: 'flex-start', marginTop: 10}}
                                        count={product.quantity}
                                        minCount={1}
                                        maxCount={30}
                                    />
                                </View>
                            </View>
                        ))}
                    </View>
                ))}
            />
            <View style={{width: '100%', backgroundColor: '#FFF', height: 150, position: 'absolute', bottom: 0, borderTopRightRadius: 24, borderTopLeftRadius: 24, padding: 20}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>Item</Text>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>0</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 15}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>Total</Text>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>Rp 0</Text>
                </View>
                <Button 
                    title="Pesan Sekarang"
                />
            </View>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Keranjang",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default CartScreen;