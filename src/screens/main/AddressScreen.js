import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar, FlatList } from 'react-native';

import { Size, Fonts, Icons, Colors } from '@styles/index';
import { Button } from '@components/global';

const AddressScreen = props => {

    const data = [{
        id: '1',
        title: 'Rumah Putih',
        address: 'Jl. Pujotomo, Glagak, Sumberrejo, Kec. Mertoyudan, Kab. Magelang, Jawa Tengah',
        phone: '+62 85660889666',
        selected: true
    }, {
        id: '2',
        title: 'Perumahan Pacul Permai',
        address: 'Jl. Pujotomo, Glagak, Sumberrejo, Kec. Mertoyudan, Kab. Magelang, Jawa Tengah',
        phone: '+62 85660889666',
        selected: false
    }]

    return (
        <View style={styles.screen}>
            <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
            <FlatList 
                data={data}
                keyExtractor={item => item.id}
                renderItem={(itemData) => (
                    <View style={[styles.addressContainer, itemData.item.selected && styles.addressContainerSelected]}>
                        <Text allowFontScaling={false} style={styles.addressTitle}>{itemData.item.title}</Text>
                        <Text allowFontScaling={false} style={styles.address}>{itemData.item.address}</Text>
                        <Text allowFontScaling={false} style={styles.addressPhone}>{itemData.item.phone}</Text>
                        <View allowFontScaling={false} style={styles.addressFooter}>
                            <Button 
                                style={{paddingHorizontal: Size.scaleSize(25)}}
                                title="Edit"
                            />
                            <Icons.TrashSVG 
                                color={Colors.primary}
                                size={Size.scaleSize(30)}
                                style={{marginRight: Size.scaleSize(20)}}
                            />
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Icons.CircleCheckSVG 
                                    size={Size.scaleSize(24)}
                                    color={itemData.item.selected? Colors.secondary : 'transparent'}
                                    style={{marginRight: Size.scaleSize(10)}}
                                />
                                <Text 
                                    allowFontScaling={false}
                                    style={[styles.addressSelected, !itemData.item.selected && { color: 'transparent' }]}
                                >
                                    Alamat Utama
                                </Text>
                            </View>
                        </View>
                    </View>
                )}
            />
            <Button 
                containerStyle={{marginHorizontal: Size.scaleSize(15), backgroundColor: '#fff', marginBottom: Size.scaleSize(10)}}
                style={{borderWidth: 2, borderColor: Colors.primary}}
                pressColor={'#FFF'}
                textStyle={{color: Colors.primary}}
                title="+ Alamat Baru" 
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    addressContainer: {
        backgroundColor: '#FFF', 
        borderWidth: 2, 
        borderRadius: 16, 
        borderColor: '#F2F4F5', 
        marginHorizontal: Size.scaleSize(10), 
        padding: Size.scaleSize(20),
        marginTop: Size.scaleSize(20)
    },
    addressContainerSelected: {
        backgroundColor: '#FFF9F3', 
        borderColor: Colors.secondary, 
    },
    addressTitle: {
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16), 
        color: '#333'
    },
    address: {
        fontFamily: Fonts.mulishRegular, 
        fontSize: Size.scaleFont(14), 
        color: '#B4B6B8', 
        marginVertical: Size.scaleSize(10)
    },
    addressPhone: {
        fontFamily: Fonts.mulishRegular, 
        fontSize: Size.scaleFont(14), 
        color: '#B4B6B8'
    },
    addressFooter: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: Size.scaleSize(10), 
        justifyContent: 'space-between'
    },
    addressSelected: {
        fontFamily: Fonts.mulishRegular,
        color: Colors.secondary,
        fontSize: Size.scaleFont(14)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Pilih Alamat",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default AddressScreen;