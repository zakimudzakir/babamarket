import React from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableHighlight, StatusBar } from 'react-native';

import { Header, InputText } from '@components/global';
import { Size, Fonts, Icons, Images, Colors } from '@styles/index';

const ChatScreen = props => {

    const data = [{
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }, {
        logo: Images.Store,
        name: 'Sejahtera Bangun',
        message: 'Tersisa 5, silahkan diorder kak.',
        time: '08:32',
        isRead: false,
        count: 1
    }, {
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }, {
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }]

    return (
        <View style={styles.screen}>
            <StatusBar 
                backgroundColor={Colors.primary}
                barStyle="light-content"
            />
            <InputText 
                containerStyle={{marginHorizontal: Size.scaleSize(18), marginVertical: Size.scaleSize(10)}}
                containerInputStyle={{backgroundColor: 'white', borderColor: '#F2F4F5'}}
                placeholder="Cari Pesan"
                rightIcon={<Icons.SearchSVG 
                    size={24}
                    color="black"
                />}
            />
            <FlatList 
                data={data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={(itemData) => (
                    <TouchableHighlight onPress={() => props.navigation.navigate('ChatDetail')} underlayColor="#efefef">
                        <View style={{flexDirection: 'row', alignItems: 'center', paddingVertical: Size.scaleSize(15), borderBottomColor: '#F2F4F5', borderBottomWidth: 1, marginHorizontal: Size.scaleSize(15) }}>
                            <View style={{flex: 1}}>
                                <Image 
                                    source={itemData.item.logo}
                                    style={{width: Size.scaleSize(50), height: Size.scaleSize(50)}}
                                />
                            </View>
                            <View style={{flex: 4 }}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333333', fontSize: Size.scaleFont(14), marginBottom: Size.scaleSize(10)}}>{itemData.item.name}</Text>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: itemData.item.isRead? '#B4B6B8' : '#333333', fontSize: Size.scaleFont(12)}}>{itemData.item.message}</Text>
                            </View>
                            <View style={{alignItems: 'center', alignSelf: 'flex-start'}}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(12), marginTop: Size.scaleSize(5), marginBottom: 5}}>{itemData.item.time}</Text>
                                {!itemData.item.isRead && <View style={{backgroundColor: Colors.secondary, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#FFF', fontSize: Size.scaleFont(12)}}>{itemData.item.count}</Text>
                                </View>}
                            </View>
                        </View>
                    </TouchableHighlight>
                )}
            />
        </View>
    )
}

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Obrolan</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    }
})

export default ChatScreen;