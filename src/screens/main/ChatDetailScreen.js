import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, StatusBar, SafeAreaView, Dimensions, TouchableOpacity } from 'react-native';
import AndroidKeyboardAdjust from 'react-native-android-keyboard-adjust';

import { Images, Size, Fonts, Icons, Colors } from '@styles/index';
import { Button, InputText, ReversedList } from '@components/global';

const ChatDetailScreen = props => {

    const [sendText, setSendText] = useState('');

    useEffect(() => {
        AndroidKeyboardAdjust.setAdjustResize();
    }, [])

    const messages = [{
        id: '1',
        user: 'other',
        isRead: true,
        time: '20:45',
        message: 'Apa masih ada kak?',

    }, {
        id: '2',
        user: 'me',
        isRead: true,
        time: '20:46',
        message: 'Tersisa 5, silahkan diorder kak.',

    }, {
        id: '3',
        user: 'other',
        isRead: true,
        time: '20:47',
        message: 'Baik Saya mau order',

    }, {
        id: '4',
        user: 'me',
        isRead: false,
        time: '20:49',
        message: 'Oke siap silakan diorder kak',

    }];


    return (
        <SafeAreaView style={styles.screen}>
            <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
            <View style={styles.productContainer}>
                <View style={styles.productImageContainer}>
                    <Image 
                        source={Images.Product}
                        style={styles.productImage}
                    />
                    
                </View>
                <View style={styles.productDetailContainer}>
                    <Text allowFontScaling={false} style={styles.productName}>Semen Gresik 40-50 kg</Text>
                    <Text allowFontScaling={false} style={styles.productPrice}>Rp 57.000</Text>
                    <View style={styles.productDetailBtn}>
                        <Button 
                            containerStyle={{
                                backgroundColor: '#FFF', 
                                borderRadius: 8
                            }}
                            style={{
                                backgroundColor: '#FFF', 
                                borderWidth: 2, 
                                borderColor: Colors.primary, 
                                paddingVertical: Size.scaleSize(8), 
                                paddingHorizontal: Size.scaleSize(8),
                                borderRadius: 8
                            }}
                            textComponent={(
                                <Icons.CartSVG 
                                    size={Size.scaleSize(22)}
                                    color={Colors.primary}
                                />
                            )}
                        />
                        <Button 
                            containerStyle={{marginLeft: Size.scaleSize(10), borderRadius: 8}}
                            style={{
                                paddingHorizontal: Size.scaleSize(25), 
                                borderRadius: 8,
                                borderWidth: 2,
                                borderColor: Colors.primary
                            }}
                            textStyle={{fontSize: Size.scaleSize(12), paddingVertical: 12}}
                            title="Beli Sekarang"
                        />
                    </View>
                </View>
            </View>
            <View style={styles.chatContainer}>
                <ReversedList
                    data={messages}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={(itemData) => {
                        if(itemData.item.user === 'me'){
                            return (
                                <View style={{marginHorizontal: 15, marginVertical: 10, marginLeft: 30, alignSelf: 'flex-end'}}>
                                    <View style={{backgroundColor: Colors.secondary, padding: 10, borderTopRightRadius: 8, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}}>
                                        <Text selectable allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#FFF'}}>{itemData.item.message}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4, alignSelf: 'flex-end'}}>
                                        <Icons.CheckSVG 
                                            size={Size.scaleSize(20)}
                                            color={itemData.item.isRead? Colors.secondary : '#B4B6B8'}
                                            style={{marginRight: 5}}
                                        />
                                        <Text allowFontScaling={false} style={{textAlign: 'right', color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(12)}}>{itemData.item.time}</Text>
                                    </View>
                                </View>
                            )
                        }else{
                            return (
                                <View style={{marginHorizontal: 15, marginVertical: 10, marginRight: 30, alignSelf: 'flex-start'}}>
                                    <View style={{backgroundColor: '#FFF', padding: 10, borderTopRightRadius: 8, borderTopLeftRadius: 8, borderBottomRightRadius: 8}}>
                                        <Text selectable allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#333'}}>{itemData.item.message}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4, alignSelf: 'flex-start'}}>
                                        <Text allowFontScaling={false} style={{textAlign: 'left', color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(12)}}>{itemData.item.time}</Text>
                                        <Icons.CheckSVG 
                                            size={Size.scaleSize(20)}
                                            color={itemData.item.isRead? Colors.secondary : '#B4B6B8'}
                                            style={{marginLeft: 5}}
                                        />
                                    </View>
                                </View>
                            )
                        }
                    }}
                />
            </View>
            <View style={[styles.chatInputContainer]}>
                <InputText 
                    containerStyle={{width: '80%'}}
                    placeholder="Tulis Pesan.."
                    multiline={true}
                    value={sendText}
                    onChangeText={text => setSendText(text)}
                />
                
                <Button 
                    disabled={!sendText}
                    containerStyle={{
                        backgroundColor: !sendText? '#ccc' : Colors.primary, 
                        borderRadius: 8
                    }}
                    style={{
                        backgroundColor: !sendText? '#ccc' : Colors.primary, 
                        paddingVertical: Size.scaleSize(10), 
                        paddingHorizontal: Size.scaleSize(10),
                        borderRadius: 8
                    }}
                    textComponent={(
                        <Icons.SendSVG 
                            size={Size.scaleSize(22)}
                            color={'#FFF'}
                        />
                    )}
                />
            </View>
            
            
        </SafeAreaView>


        // <SafeAreaView style={styles.screen}>
        //     <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
        //     <View style={styles.productContainer}>
        //         <View style={styles.productImageContainer}>
        //             <Image 
        //                 source={Images.Product}
        //                 style={styles.productImage}
        //             />
                    
        //         </View>
        //         <View style={styles.productDetailContainer}>
        //             <Text allowFontScaling={false} style={styles.productName}>Semen Gresik 40-50 kg</Text>
        //             <Text allowFontScaling={false} style={styles.productPrice}>Rp 57.000</Text>
        //             <View style={styles.productDetailBtn}>
        //                 <Button 
        //                     containerStyle={{
        //                         backgroundColor: '#FFF', 
        //                         borderRadius: 8
        //                     }}
        //                     style={{
        //                         backgroundColor: '#FFF', 
        //                         borderWidth: 2, 
        //                         borderColor: Colors.primary, 
        //                         paddingVertical: Size.scaleSize(8), 
        //                         paddingHorizontal: Size.scaleSize(8),
        //                         borderRadius: 8
        //                     }}
        //                     textComponent={(
        //                         <Icons.CartSVG 
        //                             size={Size.scaleSize(22)}
        //                             color={Colors.primary}
        //                         />
        //                     )}
        //                 />
        //                 <Button 
        //                     containerStyle={{marginLeft: Size.scaleSize(10), borderRadius: 8}}
        //                     style={{
        //                         paddingHorizontal: Size.scaleSize(25), 
        //                         borderRadius: 8,
        //                         borderWidth: 2,
        //                         borderColor: Colors.primary
        //                     }}
        //                     textStyle={{fontSize: Size.scaleSize(12), paddingVertical: 12}}
        //                     title="Beli Sekarang"
        //                 />
        //             </View>
        //         </View>
        //     </View>
        //     <View style={styles.chatContainer}>
        //         <FlatList 

        //         />
            
        //         <ScrollView style={{paddingBottom: 80}}>
        //         <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text>
        //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
        //             <Text style={{color: 'red'}}>asdssasdsasdas</Text>
        //         </ScrollView>
        //     </View>
        //     <View ref={vuewRef}>
        //     <KeyboardAwareScrollView
                
        //         // extraScrollHeight={40}
        //         // extraHeight={0}
        //         enableOnAndroid={true}
        //     >
            
        //     <Animated.View style={styles.chatInputContainer} ref={innerRef}>
                
        //         <InputText 
        //             // innerRef={innerRef}
        //             containerStyle={{width: '80%'}}
        //             placeholder="Tulis Pesan.."
        //             multiline={true}
        //             // style={{marginBottom: 16}}
        //             // containerInputStyle={{borderWidth: 0}}
        //             // style={{paddingVertical: 0}}
        //             // style={{paddingBottom: 20}}
        //         />
                
        //         <Button 
        //             disabled={false}
        //             containerStyle={{
        //                 backgroundColor: true? '#ccc' : Colors.primary, 
        //                 borderRadius: 8
        //             }}
        //             style={{
        //                 backgroundColor: true? '#ccc' : Colors.primary, 
        //                 paddingVertical: Size.scaleSize(10), 
        //                 paddingHorizontal: Size.scaleSize(10),
        //                 borderRadius: 8
        //             }}
        //             textComponent={(
        //                 <Icons.SendSVG 
        //                     size={Size.scaleSize(22)}
        //                     color={'#FFF'}
        //                 />
        //             )}
        //         />
        //     </Animated.View>
            
        //     </KeyboardAwareScrollView>
        //     </View>
            
            
        // </SafeAreaView>
        
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    productContainer: {
        backgroundColor: '#FFF',
        marginHorizontal: Size.scaleSize(20),
        margin: Size.scaleSize(20),
        paddingHorizontal: Size.scaleSize(12),
        paddingVertical: Size.scaleSize(18),
        borderRadius: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    productImageContainer: {
        borderColor: '#F2F4F5',
        width: Size.scaleSize(64),
        height: Size.scaleSize(64),
        borderRadius: 8,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    productImage: {
        width: Size.scaleSize(60),
        height: Size.scaleSize(60)
    },
    productDetailContainer: {
        marginLeft: Size.scaleSize(10),
        width: '75%'
    },
    productDetailBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: Size.scaleSize(10)
    },
    productName: {
        color: '#333',
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleSize(14)
    },
    productPrice: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(13),
        marginTop: Size.scaleSize(5)
    },
    chatContainer: {
        flex: 1
    },
    chatInputContainer: {
        padding: Size.scaleSize(16),
        paddingHorizontal: Size.scaleSize(16),
        paddingTop: Size.scaleSize(16),
        backgroundColor: '#FFF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})


export const screenOptions = navData => {
    return {
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        ),
        headerTitle: () => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image 
                    source={Images.Store}
                    style={{width: Size.scaleSize(32), height: Size.scaleSize(32)}}
                />
                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, marginLeft: Size.scaleSize(10), width: Size.scaleSize(Dimensions.get('screen').width * 0.5), fontSize: Size.scaleFont(14)}} numberOfLines={1}>Sejahtera Bangun</Text>
            </View>
        ),
        headerTitleAlign: 'left'
    }
}

export default ChatDetailScreen