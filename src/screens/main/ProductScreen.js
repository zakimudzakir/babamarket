import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity, StatusBar, FlatList } from 'react-native';
import { useDispatch } from 'react-redux';

import { Size, Fonts, Icons, Colors, Images } from '@styles/index';
import { ImageSlider, SpinnerInputNumber, Button } from '@components/global';
import Product from '@components/part/Product';
import { getProductBuyerDetail } from '@store/actions';
import { rupiahFormat } from '@helpers/functionFormat';

const ProductScreen = props => {

    const data = props.route.params && props.route.params.product? props.route.params.product : {};

    const images = [{
        filename: 'https://images.unsplash.com/photo-1453728013993-6d66e9c9123a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dmlld3xlbnwwfHwwfHw%3D&w=1000&q=80'
    }, {
        filename: 'https://images.unsplash.com/photo-1453728013993-6d66e9c9123a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dmlld3xlbnwwfHwwfHw%3D&w=1000&q=80'
    }, {
        filename: 'https://images.unsplash.com/photo-1453728013993-6d66e9c9123a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dmlld3xlbnwwfHwwfHw%3D&w=1000&q=80'
    }]

    const products = [{id: '1'}, {id: '2'}, {id: '3'}];

    const [product, setProduct] = useState({
        id: data.id,
        type: data.type,
        name: data.name,
        images: [{
            filename: null
        }],
        shop: {
            id: '',
            name: '',
            province: '',
            city: ''
        },
        category: [],
        shipping: null,
        merk: null,
        price: null,
        description: '',
        stock: null,
        relatedProducts: [],
    //     "id": 2,
    //     "type": "material",
    //     "name": "Contoh Produk 1",
    //     "photo": [
    //         {
    //             "id": 18,
    //             "url": "https://babamarket.layana.id/files/product_images/PRODUCT1-1906174936-1.png"
    //         }
    //     ],
    //     "shop": {
    //         "id": 1,
    //         "name": "Toko Testing",
    //         "province": "DI Yogyakarta",
    //         "city": "Aceh Jaya"
    //     },
    //     "area_coverage": [
    //         {
    //             "city_id": "211",
    //             "city_name": "Aceh Jaya",
    //             "subdistric_name": "Bambang Lipuro"
    //         },
    //         {
    //             "city_id": "211",
    //             "city_name": "Aceh Jaya",
    //             "subdistric_name": "Imogiri"
    //         },
    //         {
    //             "city_id": "210",
    //             "city_name": "Aceh Jaya",
    //             "subdistric_name": "Berbah"
    //         },
    //         {
    //             "city_id": "210",
    //             "city_name": "Aceh Jaya",
    //             "subdistric_name": "Sleman"
    //         },
    //         {
    //             "city_id": "213",
    //             "city_name": "Aceh Jaya",
    //             "subdistric_name": "Wonosari"
    //         }
    //     ],
    //     "category": [
    //         {
    //             "id": 2,
    //             "name": "Ubin",
    //             "type": "main",
    //             "parent_id": null
    //         },
    //         {
    //             "id": 7,
    //             "name": "Granit/Porcelain",
    //             "type": "sub",
    //             "parent_id": 2
    //         },
    //         {
    //             "id": 35,
    //             "name": "120x120 cm",
    //             "type": "sub-sub",
    //             "parent_id": 7
    //         }
    //     ],
    //     "merk": "Merk B",
    //     "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    //     "video_url": "https://youtu.be/rb2jzqJowLk",
    //     "price": 7000,
    //     "stock": 100,
    //     "shipping": null,
    //     "related_products": [
    //         {
    //             "id": 2,
    //             "photo": "https://babamarket.layana.id/files/product_images/PRODUCT1-1906174936-1.png",
    //             "type": "material",
    //             "name": "Contoh Produk 1",
    //             "price": 7000,
    //             "stock": 100,
    //             "location": "Aceh Jaya"
    //         }
    //     ]
    // }
    })
    const [readMore, setReadMore] = useState(false);
    const [stock, setStock] = useState("1");

    const dispatch = useDispatch();

    useEffect(() => {
        loadData();
    }, [])

    const loadData = async () => {
        try {
            const resData = await dispatch(getProductBuyerDetail(data.id));
            let images = [];
            resData.data.photo.map(image => {
                images.push({
                    id: image.id,
                    filename: image.url
                })
            })
            setProduct({
                id: resData.data.id,
                type: resData.data.type,
                name: resData.data.name,
                images: images,
                shop: resData.data.shop,
                category: resData.data.category,
                shipping: resData.data.shipping,
                merk: resData.data.merk,
                price: resData.data.price,
                description: resData.data.description,
                stock: resData.data.stock,
                relatedProducts: resData.data.related_products,
            })
        } catch(error) {
            console.log(error);
        }
    }

    return (
        <View style={styles.screen}>
            <StatusBar 
                backgroundColor="#FFF"
                barStyle="dark-content"
            />
            <ScrollView>
                <ImageSlider 
                    mainImageStyle={{backgroundColor: '#fff'}}
                    images={product.images}
                />
                <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icons.LocationSVG 
                            color={'#B4B6B8'}
                            size={Size.scaleSize(18)}
                            style={{marginRight: 5}}
                        />
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', fontSize: Size.scaleFont(14)}}>{`${product.shop.city}, ${product.shop.province}`}</Text>
                    </View>
                    <Text allowFontScaling={false} style={{marginVertical: Size.scaleSize(10), fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(14), color: '#333'}}>{product.name}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', marginBottom: Size.scaleSize(10)}}>
                        <View>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(12), color: '#B4B6B8'}}>Stok</Text>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333', marginTop: Size.scaleSize(5)}}>{product.stock} Tersisa</Text>
                        </View>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333'}}>Jumlah</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: Colors.primary, fontSize: Size.scaleFont(16)}}>{rupiahFormat(product.price? product.price : '0' )}</Text>
                        <SpinnerInputNumber
                            count={stock}
                            onChange={(value) => setStock(value)}
                            minCount={1}
                            maxCount={product.stock}
                        />
                    </View>
                </View>
                <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14), marginBottom: Size.scaleSize(10)}}>Pilih Ukuran</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap'}}>
                        <View style={{paddingVertical: 10, paddingHorizontal: 20, borderColor: '#F2F4F5', borderWidth: 1, borderRadius: 100, marginRight: 5, marginTop: 10}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>40 Kg</Text>
                        </View>
                        <View style={{paddingVertical: 10, paddingHorizontal: 20, borderColor: '#F2F4F5', borderWidth: 1, borderRadius: 100, marginRight: 5, marginTop: 10}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>50 Kg</Text>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Image 
                            source={Images.Store}
                            style={{width: Size.scaleSize(40), height: Size.scaleSize(40)}}
                        />
                        <View style={{marginLeft: Size.scaleSize(10)}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>{product.shop.name}</Text>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: Size.scaleSize(5)}}>
                                <Icons.LocationSVG 
                                    color={"#B4B6B8"}
                                    size={Size.scaleSize(18)}
                                    style={{marginRight: Size.scaleSize(5)}}
                                />
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', fontSize: Size.scaleFont(12)}}>Kota Yogyakarta</Text>
                            </View>
                        </View>
                    </View>
                    <Button 
                        containerStyle={{backgroundColor: 'white', borderColor: '#F2F4F5', borderWidth: 1}}
                        style={{paddingHorizontal: 20}}
                        textStyle={{fontSize: Size.scaleFont(12), color: Colors.primary}}
                        title="Lihat Toko"
                    />
                </View>
                <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(16), marginBottom: Size.scaleSize(20)}}>Spesifikasi</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#BDBDBD', fontSize: Size.scaleFont(14)}}>Kategori</Text>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>{product.type}</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#BDBDBD', fontSize: Size.scaleFont(14)}}>Merk</Text>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>{product.merk}</Text>
                    </View>
                    {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#BDBDBD', fontSize: Size.scaleFont(14)}}>Berat</Text>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>50 kg</Text>
                    </View> */}
                </View>
                <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(16), marginBottom: Size.scaleSize(20)}}>Informasi Produk</Text>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14)}}>
                        {readMore? product.description : product.description.substr(0, 250)}
                    </Text>
                    {product.description.length > 250 && (
                        <TouchableOpacity style={{marginTop: Size.scaleSize(10), alignSelf: 'flex-start'}} onPress={() => setReadMore(!readMore)}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishBold, color: Colors.primary, fontSize: Size.scaleFont(14)}}>{readMore? "Sembunyikan" : "Baca Selengkapnya"}</Text>
                                <Icons.ChevronDown 
                                    size={Size.scaleSize(24)}
                                    color={Colors.primary}
                                    style={[{marginLeft: Size.scaleSize(5)}, readMore && {transform: [{ rotate: '180deg'}]}]}
                                />
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
                <View style={{backgroundColor: '#FFF', paddingVertical: 20, marginBottom: 20}}>
                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(16), marginBottom: Size.scaleSize(20), marginHorizontal: 18}}>Produk Serupa</Text>
                    <FlatList 
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={product.relatedProducts}
                        keyExtractor={item => item.id}
                        renderItem={(itemData) => (
                            <Product 
                                containerStyle={itemData.index === products.length - 1 && {marginRight: Size.scaleSize(15)}}
                                data={itemData.item}
                                onPress={() => props.navigation.push('Product', {
                                    product: itemData.item
                                })}
                            />
                        )}
                    />
                </View>
            </ScrollView>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', paddingVertical: 10, backgroundColor: '#FFF'}}>
                <Button 
                    containerStyle={{width: '15%', backgroundColor: '#FFF' }}
                    style={{paddingVertical: Size.scaleSize(8), borderWidth: 2, borderColor: Colors.primary}}
                    textComponent={(
                        <Icons.ChatSVG 
                            color={Colors.primary}
                            size={Size.scaleSize(24)}
                            
                        />
                    )}
                    onPress={() => props.navigation.navigate('ChatDetail')}
                />
                <Button 
                    containerStyle={{width: '75%'}}
                    style={{borderWidth: 2, borderColor: Colors.primary}}
                    textComponent={(
                        <View style={{flexDirection: 'row', alignItems: 'center', paddingVertical: Size.scaleSize(8)}}>
                            <Icons.CartSVG 
                                color={"#FFF"}
                                size={Size.scaleSize(24)}
                                style={{marginRight: Size.scaleSize(10)}}
                            />
                            <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Tambah ke Keranjang</Text>
                        </View>
                    )}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#F7F9F8'
    }

})

export const screenOptions = navData => {
    return {
        headerTitle: "Detail Produk",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default ProductScreen;