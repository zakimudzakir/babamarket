import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native';

import { Icons, Colors, Fonts, Images, Size } from '@styles/index';
import { Button } from '@components/global';

const DetailTarikDanaScreen = props => {
    return (
        <View style={styles.screen}>
            <StatusBar backgroundColor={"#FFF"} barStyle="dark-content"/>
            <Icons.DownloadSVG 
                style={{marginTop: 30, marginBottom: 10}}
                size={80}
                color={Colors.secondary}
            />
            <Text style={{fontFamily: Fonts.mulishExtraBold, fontSize: 16}}>Dalam Proses</Text>
            <Text style={{fontFamily: Fonts.mulishRegular}}>25 Juni 2022 • 16:40</Text>
            <Text style={{alignSelf: 'flex-start', fontFamily: Fonts.mulishExtraBold, marginHorizontal: 15, marginTop: 20, marginBottom: 10}}>Transfer ke</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFF9F3', alignSelf: 'flex-start', marginHorizontal: 15, width: '90%', padding: 20}}>
                <Image
                    source={Images.BankBCA}
                    style={{width: 40, height: 40, marginRight: 20}}
                />
                <View>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, marginBottom: 5}}>Bank BCA</Text>
                    <Text style={{fontFamily: Fonts.mulishRegular}}>Alycia Debnam Carey</Text>
                    <Text style={{fontFamily: Fonts.mulishRegular}}>743984227</Text>
                </View>
            </View>
            <Text style={{alignSelf: 'flex-start', fontFamily: Fonts.mulishExtraBold, marginHorizontal: 15, marginTop: 20, marginBottom: 10}}>Rincian</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 10}}>
                <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Jumlah Penarikan</Text>
                <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>Rp 500,000</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 30}}>
                <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Biaya Transaksi</Text>
                <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>-Rp 2,500</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 40}}>
                <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Total Penarikan</Text>
                <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>Rp 497,500</Text>
            </View>
            <Button 
                containerStyle={{marginHorizontal: Size.scaleSize(15), backgroundColor: '#fff', marginBottom: Size.scaleSize(10), width: '90%'}}
                style={{borderWidth: 2, borderColor: Colors.primary}}
                pressColor={"#FFF"}
                textStyle={{color: Colors.primary}}
                title="Batalkan"
            />
            <Button 
                onPress={() => props.navigation.goBack()}
                containerStyle={{marginHorizontal: Size.scaleSize(15), backgroundColor: '#fff', marginBottom: Size.scaleSize(10), width: '90%'}}
                // style={{borderWidth: 2, borderColor: Colors.primary}}
                pressColor={"#FFF"}
                textStyle={{color: Colors.primary}}
                title="Kembali"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Detail",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default DetailTarikDanaScreen;