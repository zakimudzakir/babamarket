import React from 'react';
import { ScrollView, View, Text, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { BarChart } from "react-native-chart-kit";

import { Colors, Fonts, Size, Icons} from '@styles/index';
import { Header, Button } from '@components/global';

const DashboardScreen = props => {

    const data = {
        labels: ["10 Mar", "11 Mar", "12 Mar", "13 Mar", "14 Mar", "15 Mar", "16 Mar"],
        datasets: [{
            data: [50, 45, 28, 30, 49, 0, 0]
        }]
    };

    return (
        <ScrollView style={{backgroundColor: '#FFF'}}>
            <StatusBar backgroundColor={Colors.primary} barStyle="light-content"/>
            <View style={styles.screen}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <View style={{flex: 1, height: 50, backgroundColor: '#FFF9F3', alignItems: 'center', justifyContent: 'center', marginHorizontal: 10, borderRadius: 40, borderWidth: 1.5, borderColor: Colors.secondary}}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: Colors.secondary}}>7 Hari</Text>
                    </View>
                    <View style={{flex: 1, height: 50, backgroundColor: '#F7F9F8', alignItems: 'center', justifyContent: 'center', marginHorizontal: 10, borderRadius: 40, borderWidth: 1.5, borderColor: '#F7F9F8'}}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333333'}}>30 Hari</Text>
                    </View>
                    <View style={{flex: 1, height: 50, backgroundColor: '#F7F9F8', alignItems: 'center', justifyContent: 'center', marginHorizontal: 10, borderRadius: 40, borderWidth: 1.5, borderColor: '#F7F9F8'}}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: '#333333'}}>Semua</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', marginHorizontal: 10, marginTop: 20}}>
                    <View>
                        <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', fontSize: Size.scaleFont(16), marginBottom: Size.scaleSize(5)}}>Total Pendapatan</Text>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', marginRight: 5, marginTop: Size.scaleSize(3)}}>Rp</Text>
                            <Text style={{fontSize: Size.scaleFont(24), color: '#333333', fontFamily: Fonts.mulishExtraBold}}>25.500.000</Text>
                        </View>
                    </View>
                    <Button 
                        onPress={() => props.navigation.navigate('TarikDana')}
                        containerStyle={{marginBottom: Size.scaleSize(8)}}
                        textComponent={(
                            <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10}}>
                                <Icons.LockedSVG
                                    size={25} 
                                    color="#FFF"
                                />
                                <Text style={{fontFamily: Fonts.mulishBold, color: '#FFF', marginLeft: 10, marginTop: 2}}>Tarik Dana</Text>
                            </View>
                        )}
                    />
                </View>
                
                <BarChart
                    style={{marginTop: 20, left: -35 }}
                    showBarTops={true}
                    // showValuesOnTopOfBars={true}
                    showBarTops={false}
                    showLegend={false}
                    withHorizontalLabels={false}
                    data={data}
                    width={Dimensions.get("window").width}
                    height={180}
                    chartConfig={{
                        backgroundColor: "#FFF",
                        backgroundGradientFrom: "#FFF",
                        backgroundGradientTo: "#FFF",
                        color: (opacity = 1) => Colors.opacityColor(Colors.secondary, opacity),
                        labelColor: (opacity = 1) => Colors.opacityColor("#B4B6B8", opacity),
                        strokeWidth: 0,
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#B4B6B8"
                        },
                        barRadius: 15,
                        useShadowColorFromDataset: false,
                        fillShadowGradientFromOpacity: 1,
                        fillShadowGradientFromOffset: 1,
                        useShadowColorFromDataset: false,
                        stackedBar: true,
                        propsForVerticalLabels: {
                            fontFamily: Fonts.mulishRegular
                        }
                        // formatXLabel: (xLabel) => <Text>{xLabel}</Text>
                    }}
                />

                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.FileCheckSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Total Penjualan</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>110 terjual</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}>90%</Text>
                </View>
                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.FileCheckSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Pesanan Terbaru</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>5 pesanan</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}></Text>
                </View>
                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.BoxSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Total Produk</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>25 produk</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}></Text>
                </View>
            </View>
        </ScrollView>
    )
}

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14)}}>Selamat Datang,</Text>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Jaya Sejahtera</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    }
})

export default DashboardScreen;