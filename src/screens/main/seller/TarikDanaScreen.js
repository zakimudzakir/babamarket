import React, { useState } from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList, TouchableHighlight, Image, TouchableOpacity, StatusBar } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Button, InputText } from '@components/global';
import { Size, Fonts, Colors, Icons, Images } from '@styles/index';

const FirstRoute = () => {

    const [selected, setSelected] = useState(0);

    const data = [{
        id: 'bca',
        name: 'Bank BCA',
        noRekening: '743984227',
        image: Images.BankBCA
    }, {
        id: 'mandiri',
        name: 'Bank Mandiri',
        noRekening: '8344749487014',
        image: Images.BankMandiri
    }]
    return (
        <View style={styles.screen}>
            <FlatList 
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                keyExtractor={item => item.id}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <Text allowFontScaling={false} style={styles.title}>Nominal Penarikan</Text>
                        <InputText 
                            placeholder={"Contoh: 1.000.000"}
                        />
                        <Text allowFontScaling={false} style={[styles.title, { marginTop: 20, marginBottom: 10 }]}>Rekening Tujuan</Text>
                    </View>
                )}
                renderItem={(itemData) => (
                    <View style={{marginBottom: 20, borderRadius: 20}}>
                        <TouchableHighlight 
                            style={{borderRadius: 20}}
                            onPress={() => setSelected(itemData.index)}
                            underlayColor={Colors.secondary}
                        >
                            <View style={{backgroundColor: selected === itemData.index? '#FFF9F3' : '#FFF', borderColor: selected === itemData.index? Colors.secondary : '#F2F4F5', borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderRadius: 20, padding: 20}}>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Image 
                                        source={itemData.item.image}
                                        style={{width: 40, height: 40, marginRight: 10}}
                                    />
                                    <View>
                                        <Text style={{fontFamily: Fonts.mulishExtraBold, marginBottom: 5}}>{itemData.item.name}</Text>
                                        <Text style={{fontFamily: Fonts.mulishRegular}}>{itemData.item.noRekening}</Text>
                                    </View>
                                </View>
                                <View style={{width: 25, height: 25, borderColor: Colors.secondary, borderWidth: 2, borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
                                    {selected === itemData.index && <View style={{width: 15, height: 15, backgroundColor: Colors.secondary, borderRadius: 15}}/>}
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                )}
            />
            <Button 
                containerStyle={{marginHorizontal: 20, marginBottom: 5}}
                title="Tarik Sekarang"
            />
        </View>
    )
};
  
const SecondRoute = props => {
    
    return (
        <View style={styles.screen}>
            <StatusBar backgroundColor={"#FFF"} barStyle="dark-content"/>
            <View style={{backgroundColor: '#F7F9F8', paddingHorizontal: 20, paddingVertical: 10}}>
                <Text style={{fontFamily: Fonts.mulishRegular}}>25 Juni 2022</Text>
            </View>
            <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana')}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icons.DownloadSVG 
                            size={35}
                            color={Colors.secondary}
                        />
                        <View style={{marginLeft: 10}}>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                            <Text style={{color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: 12}}>Dalam Proses • 16:40</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>-Rp 500.000</Text>
                </View>
            </TouchableHighlight>
            <View style={{backgroundColor: '#F7F9F8', paddingHorizontal: 20, paddingVertical: 10}}>
                <Text style={{fontFamily: Fonts.mulishRegular}}>25 Mei 2022</Text>
            </View>
            <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana')}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icons.DownloadSVG 
                            size={35}
                            color={Colors.secondary}
                        />
                        <View style={{marginLeft: 10}}>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                            <Text style={{color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: 12}}>Dalam Proses • 16:40</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>-Rp 500.000</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana')}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icons.DownloadSVG 
                            size={35}
                            color={Colors.secondary}
                        />
                        <View style={{marginLeft: 10}}>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                            <Text style={{color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: 12}}>Dalam Proses • 16:40</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>-Rp 500.000</Text>
                </View>
            </TouchableHighlight>
        </View>
    )
};
  
const TarikDanaScreen = props => {

    const renderScene = SceneMap({
        first: FirstRoute,
        second: () => <SecondRoute {...props}/>
    });

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Penarikan' },
        { key: 'second', title: 'Riwayat Penarikan' }
    ]);

    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={props => (
                <TabBar 
                    {...props} 
                    indicatorStyle={styles.indidcatorStyle}
                    style={styles.tabStyle}
                    activeColor={Colors.secondary}
                    inactiveColor={'#CDCFD0'}
                    pressColor={"transparent"}
                    renderLabel={({ route, color }) => (
                        <Text 
                            allowFontScaling={false} 
                            style={[styles.tabLabel, { color: color }]}
                        >
                            {route.title}
                        </Text>
                    )}
                />
            )}
        />
    )
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        backgroundColor: Colors.secondary, 
        height: Size.scaleSize(3), 
        borderRadius: 3
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    tabLabel: {
        fontFamily: Fonts.mulishMedium,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333',
        marginBottom: 20
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Tarik Dana",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default TarikDanaScreen;