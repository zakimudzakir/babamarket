import React from 'react';
import { ScrollView, View, Text, TouchableOpacity, Image } from 'react-native';

import { Colors, Fonts, Size, Icons, Images } from '@styles/index';
import { InputText, Button } from '@components/global';

const InputProductScreen = props => {
    return (
        <View style={{flex: 1, backgroundColor: '#FFF'}}>
            <ScrollView style={{backgroundColor: '#FFF'}}>
                <View style={{flex: 1, backgroundColor: '#FFF', marginHorizontal: 15, marginTop: 20}}>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Foto Produk</Text>
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={{flexDirection: 'row', marginVertical: 10}}>
                        <TouchableOpacity style={{marginLeft: 15}}>
                            <View style={{borderColor: '#F2F4F5', width: 78, height: 78, borderWidth: 2, borderRadius: 10, alignItems: 'center', justifyContent: 'center', overflow: 'hidden'}}>
                                <Image 
                                    source={Images.Product}
                                    style={{width: 78, height: 78}}
                                />
                                <View style={{backgroundColor: '#B4B6B8', width: 15, height: 15, position: 'absolute', top: 2, right: 2, borderRadius: 15}}>
                                    <TouchableOpacity onPress={() => alert('testing')}>
                                        <Icons.PlusSVG 
                                            color={"#FFF"}
                                            size={15}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={{backgroundColor: Colors.opacityColor("#333333", 0.5), width: '100%', position: 'absolute', bottom: 0, paddingVertical: 5, alignItems: 'center'}}>
                                    <Text style={{fontFamily: Fonts.mulishRegular, fontSize: 10, color: '#FFF'}}>Foto Sampul</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{marginLeft: 15}}>
                            <View style={{borderColor: '#F2F4F5', width: 78, height: 78, borderWidth: 2, borderRadius: 10, alignItems: 'center', justifyContent: 'center', overflow: 'hidden'}}>
                                <Image 
                                    source={Images.Product}
                                    style={{width: 78, height: 78}}
                                />
                                <View style={{backgroundColor: '#B4B6B8', width: 15, height: 15, position: 'absolute', top: 2, right: 2, borderRadius: 15}}>
                                    <TouchableOpacity onPress={() => alert('testing')}>
                                        <Icons.PlusSVG 
                                            color={"#FFF"}
                                            size={15}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{marginHorizontal: 15}}>
                            <View style={{borderColor: Colors.primary, width: 78, height: 78, borderStyle: 'dashed', backgroundColor: "#FFF2F3", borderWidth: 2, borderRadius: 10, alignItems: 'center', justifyContent: 'center'}}>
                                <Icons.PlusSVG 
                                    size={28}
                                    color={Colors.primary}
                                />
                                <Text style={{fontFamily: Fonts.mulishRegular, color: Colors.primary}}>Foto</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={{width: '100%', height: 10, backgroundColor: '#F7F9F8'}}/>
                <View style={{marginVertical: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15}}>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Nama Produk</Text>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>0/255</Text>
                </View>
                <InputText 
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Nama Produk"
                />
                <View style={{marginTop: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', marginHorizontal: 15}}>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Kategori & Sub-kategori</Text>
                </View>
                <InputText 
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Kategori & Sub-kategori"
                    editable={false}
                />
                <View style={{marginTop: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15}}>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Deskripsi Produk</Text>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>0/3000</Text>
                </View>
                <InputText 
                    style={{textAlignVertical: 'top'}}
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Deskripsi Produk"
                    multiline={true}
                    numberOfLines={6}
                />
                <View style={{marginTop: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15}}>
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Link Video Produk (Opsional)</Text>
                    <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>0/255</Text>
                </View>
                <InputText 
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Ketikkan link dari youtube"
                />
                <View style={{width: '100%', height: 10, backgroundColor: '#F7F9F8', marginVertical: 15}}/>
                <View style={{marginTop: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', marginHorizontal: 15}}>
                    <Icons.ShareSVG 
                        color={"#333"}
                        size={24}
                        style={{marginRight: 10}}
                    />
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Harga (Rupiah)</Text>
                </View>
                <InputText 
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Harga"
                />
                <View style={{marginTop: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', marginHorizontal: 15}}>
                    <Icons.ShareSVG 
                        color={"#333"}
                        size={24}
                        style={{marginRight: 10}}
                    />
                    <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Stok</Text>
                </View>
                <InputText 
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Stok"
                />
                <View style={{marginTop: 60, marginBottom: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icons.ShareSVG 
                            color={"#333"}
                            size={24}
                            style={{marginRight: 10}}
                        />
                        <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Bagikan</Text>
                    </View>
                    <Icons.ChevronRight 
                        color="#333"
                        size={24}
                    />
                </View>
            </ScrollView>
            <View style={{marginVertical: 20, flexDirection: 'row', justifyContent: 'space-around'}}>
                <Button 
                    containerStyle={{width: '45%', backgroundColor: '#FFF', borderColor: Colors.primary}}
                    textStyle={{color: Colors.primary}}
                    style={{borderColor: Colors.primary, borderWidth: 2}}
                    pressColor={"#FFF"}
                    underlayColor={Colors.primary}
                    title="Arsipkan"
                />
                <Button     
                    containerStyle={{width: '45%'}}
                    style={{borderColor: Colors.primary, borderWidth: 2}}
                    title="Perbarui"
                />
            </View>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Tambah Produk",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default InputProductScreen;
