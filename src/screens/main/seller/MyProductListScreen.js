import React from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList, TouchableHighlight, StatusBar, Image, Dimensions } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Button, InputText, Header } from '@components/global';
import { Size, Fonts, Colors, Icons, Images } from '@styles/index';

const FirstRoute = props => {

    const data = [{
        id: '1',
        name: 'Semen Gresik 40-50 kg',
        harga: 'Rp 57.000',
        stock: '100',
        soldOut: '450',
        image: Images.Product
    }, {
        id: '2',
        name: 'Pintu Kamar Mandi Aluminium Galvalum',
        harga: 'Rp 195.000',
        stock: '100',
        soldOut: '450',
        image: Images.Product
    }, {
        id: '3',
        name: 'Kulkas Aqua AQR-D261(DS) 2 Pintu Dark Silver',
        harga: 'Rp 2.500.000',
        stock: '100',
        soldOut: '450',
        image: Images.Product
    }, {
        id: '4',
        name: 'Semen Gresik 40-50 kg',
        harga: 'Rp 57.000',
        stock: '100',
        soldOut: '450',
        image: Images.Product
    }, {
        id: '5',
        name: 'Pintu Kamar Mandi Aluminium Galvalum',
        harga: 'Rp 195.000',
        stock: '100',
        soldOut: '450',
        image: Images.Product
    }]
    return (
        <View style={styles.screen}>
            
            <FlatList 
                showsVerticalScrollIndicator={false}
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                keyExtractor={item => item.id}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <InputText 
                            placeholder="Cari Produk"
                            rightIcon={(
                                <Icons.SearchSVG 
                                    size={20}
                                    color="#333333"
                                />
                            )}
                        />
                        <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', marginVertical: 20}}>25 produk tersedia</Text>
                    </View>
                )}
                renderItem={(itemData) => (
                    <View style={{marginBottom: 20, borderRadius: 20, borderColor: '#F2F4F5', borderWidth: 1, padding: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                            <Image 
                                style={{width: 74, height: 74, marginRight: 20}}
                                source={itemData.item.image}
                            />
                            <View style={{flex: 1}}>
                                <Text style={{fontFamily: Fonts.mulishBold, color: '#333', marginBottom: 5}}>{itemData.item.name}</Text>
                                <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>{itemData.item.harga}</Text>
                            </View>
                        </View>
                        <View style={{width: '100%', height: 1, backgroundColor: '#F2F4F5', marginVertical: 15}}/>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 20}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Icons.ShoppingBagSVG 
                                    color={"#B4B6B8"}
                                    size={20}
                                />
                                <Text style={{fontFamily: Fonts.mulishRegular, fontSize: 12, color: '#B4B6B8', marginLeft: 5}}>Stok 100</Text>
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Icons.ShoppingBagSVG 
                                    color={"#B4B6B8"}
                                    size={20}
                                />
                                <Text style={{fontFamily: Fonts.mulishRegular, fontSize: 12, color: '#B4B6B8', marginLeft: 5}}>Terjual 450</Text>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}}>
                            <Button 
                                containerStyle={{backgroundColor: "#FFF", borderRadius: 10, borderColor: Colors.primary, borderWidth: 2}}
                                textComponent={(
                                    <View style={{width: Dimensions.get('screen').width * 0.24, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                        <Icons.BellSVG 
                                            size={24}
                                            color={Colors.primary}
                                        />
                                        <Text style={{fontFamily: Fonts.mulishExtraBold, marginLeft: 2, fontSize: 13, color: Colors.primary}}>Ubah</Text>
                                    </View>
                                )}
                            />
                            <Button 
                                containerStyle={{backgroundColor: "#FFF", borderRadius: 10, borderColor: Colors.primary, borderWidth: 2}}
                                textComponent={(
                                    <View style={{width: Dimensions.get('screen').width * 0.24, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                        <Icons.BellSVG 
                                            size={24}
                                            color={Colors.primary}
                                        />
                                        <Text style={{fontFamily: Fonts.mulishExtraBold, marginLeft: 2, fontSize: 13, color: Colors.primary}}>Arsipkan</Text>
                                    </View>
                                )}
                            />
                            <Button 
                                containerStyle={{backgroundColor: "#FFF", borderRadius: 10, borderColor: Colors.primary, borderWidth: 2}}
                                textComponent={(
                                    <View style={{width: Dimensions.get('screen').width * 0.24, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                        <Icons.TrashSVG 
                                            size={24}
                                            color={Colors.primary}
                                        />
                                        <Text style={{fontFamily: Fonts.mulishExtraBold, marginLeft: 2, fontSize: 13, color: Colors.primary}}>Hapus</Text>
                                    </View>
                                )}
                            />
                        </View>
                    </View>
                )}
            />
            <Button 
                containerStyle={{width: '90%', alignSelf: 'center', marginBottom: 10}}
                title="+ Produk Baru"
                onPress={() => props.navigation.navigate('InputProduct')}
            />
        </View>
    )
};

const MyProductListScreen = props => {

    const renderScene = SceneMap({
        first: () => <FirstRoute {...props} status="Belum Dibayar" btnColor="#FF5247"/>,
        second: () => <FirstRoute {...props} status="Dikemas" btnColor="#FFB323"/>,
        third: () => <FirstRoute {...props} status="Dikirim" btnColor="#1DA1F2"/>,
        fourth: () => <FirstRoute {...props} status="Selesai" btnColor="#23C16B"/>,
        fifth: () => <FirstRoute {...props} status="Dibatalkan" btnColor="#B4B6B8"/>
    });

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Tersedia' },
        { key: 'second', title: 'Habis' },
        { key: 'third', title: 'Diarsipkan' },
        { key: 'fourth', title: 'Bahan Bangunan' }
    ]);

    return (
        <>
            <StatusBar backgroundColor={Colors.primary} barStyle="light-content"/>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                // swipeEnabled={false}
                renderTabBar={props => (
                    <TabBar 
                        {...props} 
                        scrollEnabled={true}
                        tabStyle={{width: 'auto'}}
                        indicatorStyle={styles.indidcatorStyle}
                        style={styles.tabStyle}
                        activeColor={Colors.secondary}
                        inactiveColor={'#CDCFD0'}
                        pressColor={"transparent"}
                        renderLabel={({ route, color }) => (
                            <View style={[
                                styles.labelContainer, 
                                { borderColor: color, backgroundColor: Colors.opacityColor(color, 0.08) }
                            ]}>
                                <Text 
                                    allowFontScaling={false} 
                                    style={[styles.tabLabel, { color: color }]}
                                >
                                    {route.title}
                                </Text>
                            </View>
                        )}
                    />
                )}
            />
        </>
    )
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        backgroundColor: Colors.secondary, 
        height: 0,
        borderRadius: 3
    },
    tabStyle: {
        elevation: 0, 
        backgroundColor: '#FFF'
    },
    labelContainer: {
        borderWidth: 1, 
        padding: Size.scaleSize(10), 
        borderRadius: 30, 
        paddingHorizontal: Size.scaleSize(20)
    },
    tabLabel: {
        fontFamily: Fonts.mulishBold,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333',
        marginBottom: 20
    }
})

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Produk Saya</Text>
                    </View>
                )}
            />
        )
    }
}

export default MyProductListScreen;