import React from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList, TouchableHighlight, StatusBar } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Button, InputText, Header } from '@components/global';
import { Size, Fonts, Colors, Icons } from '@styles/index';

const FirstRoute = props => {

    const data = [{
        id: 'TVFPH12',
        date: '08/03/2022, 13:55',
        total: 'Rp 264.000',
        status: 'Belum Dibayar'
    }, {
        id: 'KLOPE22',
        date: '08/03/2022, 13:55',
        total: 'Rp 1.125.000',
        status: 'Belum Dibayar'
    }, {
        id: 'FGHTY09',
        date: '08/03/2022, 13:55',
        total: 'Rp 500.000',
        status: 'Belum Dibayar'
    }, {
        id: 'DFGTR88',
        date: '08/03/2022, 13:55',
        total: 'Rp 264.000',
        status: 'Belum Dibayar'
    }, {
        id: 'NNBDM44',
        date: '08/03/2022, 13:55',
        total: 'Rp 880.000',
        status: 'Belum Dibayar'
    }]
    return (
        <View style={styles.screen}>
            
            <FlatList 
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                keyExtractor={item => item.id}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <InputText 
                            placeholder="Cari Kode Pesan"
                            rightIcon={(
                                <Icons.SearchSVG 
                                    size={20}
                                    color="#333333"
                                />
                            )}
                        />
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: Fonts.mulishBold}}>5 Pesanan Baru</Text>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20}}>
                                <Text style={{fontFamily: Fonts.mulishRegular, color: Colors.primary, marginRight: 10}}>Semua</Text>
                                <Icons.FilterSVG 
                                    color={Colors.primary}
                                    size={24}
                                />
                            </View>
                        </View>
                    </View>
                )}
                renderItem={(itemData) => (
                    <View style={{marginBottom: 20, borderRadius: 20, borderColor: '#F2F4F5', borderWidth: 1, padding: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>{itemData.item.date}</Text>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Total</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>{itemData.item.id}</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>{itemData.item.total}</Text>
                        </View>
                        <View style={{width: '100%', height: 1, backgroundColor: '#F2F4F5', marginVertical: 15}}/>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Button 
                                containerStyle={{backgroundColor: props.btnColor, borderRadius: 10}}
                                title={props.status}
                                textStyle={{fontFamily: Fonts.mulishRegular, paddingHorizontal: 20, fontSize: 14}}
                            />
                            <View style={{backgroundColor: Colors.primary, padding: 5, borderRadius: 60}}>
                                <Icons.ChevronRight 
                                    size={22}
                                    color="#FFF"
                                />
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>
    )
};

const SellingScreen = props => {

    const renderScene = SceneMap({
        first: () => <FirstRoute {...props} status="Belum Dibayar" btnColor="#FF5247"/>,
        second: () => <FirstRoute {...props} status="Dikemas" btnColor="#FFB323"/>,
        third: () => <FirstRoute {...props} status="Dikirim" btnColor="#1DA1F2"/>,
        fourth: () => <FirstRoute {...props} status="Selesai" btnColor="#23C16B"/>,
        fifth: () => <FirstRoute {...props} status="Dibatalkan" btnColor="#B4B6B8"/>
    });

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Belum Dibayar' },
        { key: 'second', title: 'Dikemas' },
        { key: 'third', title: 'Dikirim' },
        { key: 'fourth', title: 'Selesai' },
        { key: 'fifth', title: 'Pembatalan' }
    ]);

    return (
        <>
            <StatusBar backgroundColor={Colors.primary} barStyle="light-content"/>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                // swipeEnabled={false}
                renderTabBar={props => (
                    <TabBar 
                        {...props} 
                        scrollEnabled={true}
                        tabStyle={{width: 'auto'}}
                        indicatorStyle={styles.indidcatorStyle}
                        style={styles.tabStyle}
                        activeColor={Colors.secondary}
                        inactiveColor={'#CDCFD0'}
                        pressColor={"transparent"}
                        renderLabel={({ route, color }) => (
                            <View style={[
                                styles.labelContainer, 
                                { borderColor: color, backgroundColor: Colors.opacityColor(color, 0.08) }
                            ]}>
                                <Text 
                                    allowFontScaling={false} 
                                    style={[styles.tabLabel, { color: color }]}
                                >
                                    {route.title}
                                </Text>
                            </View>
                        )}
                    />
                )}
            />
        </>
    )
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        height: 0
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    labelContainer: {
        borderWidth: 1, 
        padding: Size.scaleSize(10), 
        borderRadius: 30, 
        paddingHorizontal: Size.scaleSize(20)
    },
    tabLabel: {
        fontFamily: Fonts.mulishBold,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333',
        marginBottom: 20
    }
})

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Penjualan Saya</Text>
                    </View>
                )}
            />
        )
    }
}

export default SellingScreen;