import React from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Header, Button } from '@components/global';
import { Size, Fonts, Colors, Icons } from '@styles/index';
import OrderItem from '@components/part/OrderItem';

const FirstRoute = () => {
    const data = [{
        id: 'TVFPH12',
        status: 'Belum dibayar',
        statusType: 1,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH13',
        status: 'Belum dibayar',
        statusType: 1,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH14',
        status: 'Belum dibayar',
        statusType: 1,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH15',
        status: 'Belum dibayar',
        statusType: 1,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }]
    return (
        <View style={styles.screen}>
            <FlatList 
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                keyExtractor={item => item.id}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <Text allowFontScaling={false} style={styles.title}>1 pesanan pending</Text>
                    </View>
                )}
                renderItem={(itemData) => (
                    <OrderItem 
                        {...itemData.item}
                    />
                )}
            />
        </View>
    )
};
  
const SecondRoute = () => {
    const data = [{
        id: 'TVFPH12',
        status: 'Dikirim',
        statusType: 3,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH13',
        status: 'Sedang Disiapkan',
        statusType: 2,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH14',
        status: 'Sedang Disiapkan',
        statusType: 2,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH15',
        status: 'Dikirim',
        statusType: 3,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }]
    return (
        <View style={styles.screen}>
            <FlatList 
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <Text allowFontScaling={false} style={styles.title}>3 pesanan aktif</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: Colors.primary}}>Semua</Text>
                            <Icons.FilterSVG 
                                color={Colors.primary}
                                size={Size.scaleSize(20)}
                                style={{marginLeft: Size.scaleSize(5)}}
                            />
                        </View>
                    </View>
                )}
                renderItem={(itemData) => (
                    <OrderItem 
                        {...itemData.item}
                    />
                )}
            />
        </View>
    )
};

const ThirdRoute = () => {
    const data = [{
        id: 'TVFPH12',
        status: 'Selesai',
        statusType: 4,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH13',
        status: 'Selesai',
        statusType: 4,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }, {
        id: 'TVFPH14',
        status: 'Selesai',
        statusType: 4,
        date: 'Jun 6, 2020',
        time: '10:00',
        count: 2,
        price: 'Rp 264.000'
    }]
    return (
        <View style={styles.screen}>
            <FlatList
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={data}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        <Text allowFontScaling={false} style={styles.title}>Semua Transaksi</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: Colors.primary}}>Semua</Text>
                            <Icons.FilterSVG 
                                color={Colors.primary}
                                size={Size.scaleSize(20)}
                                style={{marginLeft: Size.scaleSize(5)}}
                            />
                        </View>
                    </View>
                )}
                renderItem={(itemData) => (
                    <OrderItem 
                        {...itemData.item}
                    />
                )}
            />
        </View>
    )
};
  
const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute
});

const OrderScreen = props => {

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Pending' },
        { key: 'second', title: 'Diproses' },
        { key: 'third', title: 'Selesai' },
    ]);

    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={props => (
                <TabBar 
                    {...props} 
                    indicatorStyle={styles.indidcatorStyle}
                    style={styles.tabStyle}
                    activeColor={Colors.secondary}
                    inactiveColor={'#CDCFD0'}
                    pressColor={"transparent"}
                    renderLabel={({ route, color }) => (
                        <Text 
                            allowFontScaling={false} 
                            style={[styles.tabLabel, { color: color }]}
                        >
                            {route.title}
                        </Text>
                    )}
                />
            )}
        />
    )
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        backgroundColor: Colors.secondary, 
        height: Size.scaleSize(3), 
        borderRadius: 3
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    tabLabel: {
        fontFamily: Fonts.mulishMedium,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333'
    }
})

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Pesanan Saya</Text>
                    </View>
                )}
            />
        )
    }
}

export default OrderScreen;