import React, { useState } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { useDispatch } from 'react-redux';

import { Size, Icons, Fonts, Colors } from '@styles/index';
import { InputText, Button } from '@components/global';
import { login, updateModal, setUserLocation, updateLoading } from '@store/actions';
import { checkPermissions, PERMISSIONS_TYPE } from '@helpers/permissions';

const LoginScreen = props => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [securePassword, setSecurePassword] = useState(true);

    const dispatch = useDispatch();

    const onLogin = async () => {
        try{
            await dispatch(login(email, password));
            await loadLocation();
        }catch(error){
            dispatch(updateModal(true, 'LOGIN ERROR', error.message))
        }
    }

    const loadLocation = async () => {
        try{
            await dispatch(updateLoading(true));
            const permissions = await checkPermissions(PERMISSIONS_TYPE.location);
            Geolocation.getCurrentPosition(userLocation => {
                console.log("userLocation", userLocation)
                dispatch(setUserLocation(userLocation.coords.latitude, userLocation.coords.longitude));
            }, error => {
                dispatch(updateModal(true, 'LOGIN ERROR', error.message))
            }, {
                enableHighAccuracy: false,
                timeout: 5000,
                maximumAge: 10000
            })
            await dispatch(updateLoading(false));
            if(permissions){
                props.navigation.replace('MainNavigation');
            } else {
                throw {
                    message: 'Pastakan anda mengizinkan lokasi untuk dapat melakukan login'
                }
            }
        }catch(error){
            dispatch(updateModal(true, 'LOGIN ERROR', error.message))
            await dispatch(updateLoading(false));
        }
    }

    return (
        <ScrollView style={{backgroundColor: 'white'}}>
            <StatusBar 
                backgroundColor="#FFF"
                barStyle="dark-content"
            />
            <View style={styles.screen}>
                <View style={{flex: 1}}>
                <Text allowFontScaling={false} style={styles.titleWelcome}>{"Selamat Datang\nKembali"}</Text>
                <Text allowFontScaling={false} style={styles.title}>Silahkan masuk ke akun Anda</Text>
                <InputText 
                    containerStyle={styles.textInputContainer}
                    leftIcon={<Icons.EmailSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    placeholder="Masukkan email"
                    label="Email"
                    value={email}
                    onChangeText={setEmail}
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    leftIcon={<Icons.LockedSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    isPassword
                    placeholder="Masukkan kata sandi"
                    label="Kata Sandi"
                    secureTextEntry={securePassword}
                    setSecureTextEntry={setSecurePassword}
                    value={password}
                    onChangeText={setPassword}
                />
                <Button 
                    containerStyle={styles.btnContainer}
                    title="Masuk"
                    onPress={onLogin}
                />
                <Button 
                    containerStyle={[styles.btnContainer, {backgroundColor: '#FFF'}]}
                    textStyle={{color: Colors.primary}}
                    underlayColor={Colors.opacityColor(Colors.primary, .1)}
                    onPress={() => props.navigation.navigate('ForgotPassword')}
                    title="Lupa Kata Sandi"
                />
                </View>
                <View style={styles.footer}>
                    <Text allowFontScaling={false} style={styles.labelFooter}>Saya belum memiliki akun? </Text>
                    <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Register')}>
                        <Text allowFontScaling={false} style={styles.labelBtnFooter}>Daftar</Text>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </ScrollView>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'Masuk ke Akun',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    titleWelcome: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#333',
        fontSize: Size.scaleFont(24),
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(30),
        marginBottom: Size.scaleSize(16)
    },
    title: {
        fontFamily: Fonts.mulishRegular,
        color: '#B4B6B8',
        fontSize: Size.scaleFont(14),
        marginHorizontal: Size.scaleSize(20),
        
    },
    textInputContainer: {
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(23)
    },
    btnContainer: {
        marginHorizontal: Size.scaleSize(20), 
        marginTop: Size.scaleSize(40)
    },
    footer: {
        flexDirection: 'row', 
        alignSelf: 'center', 
        marginTop: Size.scaleSize(150), 
        alignItems: 'center'
    },
    labelFooter: {
        fontFamily: Fonts.mulishRegular,
        color: '#333',
        fontSize: Size.scaleFont(14)
    },
    labelBtnFooter: {
        fontFamily: Fonts.mulishExtraBold, 
        color: Colors.primary,
        fontSize: Size.scaleFont(14)
    }
})

export default LoginScreen;