import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

import { Icons, Size, Fonts, Colors, Images } from '@styles/index';
import { Button } from '@components/global';

const VerificationCodeScreen = props => {
    return (
        <View style={styles.screen}>
            <Image  
                style={styles.box}
                source={Images.BabaMarket}
                tintColor={Colors.primary}
            />
            <Text allowFontScaling={false} style={styles.labelTimer}>Waktu tersisa</Text>
            <Text allowFontScaling={false} style={styles.timer}>00:35</Text>
            <Text allowFontScaling={false} style={styles.label}>Masukkan kode OTP yang dikirim{"\n"}ke email rob***@gmail.com</Text>
            <View style={styles.pinContainer}>
                <View style={{height: 60, width: '100%', backgroundColor: '#F2F4F5', marginBottom: 20, borderRadius: 7}} />
                <Button 
                    title="Lanjutkan"
                    onPress={() => props.navigation.navigate('ResetPassword')}
                />
            </View>
            <Text allowFontScaling={false} style={styles.labelCode}>Tidak menerima kode?</Text>
            <Button 
                containerStyle={styles.buttonResend}
                style={{borderWidth: 2, borderColor: Colors.primary}}
                textStyle={{color: Colors.primary}}
                pressColor="white"
                title="Kirim Ulang"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    box: {
        width: Size.scaleSize(100), 
        height: Size.scaleSize(100),
        // backgroundColor: '#C4C4C4',
        marginTop: Size.scaleSize(20),
        marginBottom: Size.scaleSize(50)
    },
    labelTimer: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        color: '#B4B6B8'
    },
    timer: {
        fontFamily: Fonts.mulishExtraBold,
        color: Colors.secondary,
        fontSize: Size.scaleFont(24),
        marginTop: Size.scaleSize(8)
    },
    label: {
        textAlign: 'center',
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(20),
        width: '80%'
    },
    pinContainer: {
        backgroundColor: 'white',
        borderRadius: 14,
        marginTop: Size.scaleSize(20),
        marginHorizontal: Size.scaleSize(20),
        padding: Size.scaleSize(16),
        width: '90%',
        shadowColor: 'rgb(15, 25, 35)',
        shadowOpacity: 0.86,
        shadowOffset: { width: 1, height: 8 },
        shadowRadius: 2,
        elevation: 2
    },
    labelCode: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(40),
        color: '#333',
    },
    buttonResend: {
        backgroundColor: '#FFF', 
        
        width: '60%',
        marginTop: Size.scaleSize(20)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Verifikasi Kode OTP',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default VerificationCodeScreen;