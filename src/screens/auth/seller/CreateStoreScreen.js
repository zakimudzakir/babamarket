import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { Fonts, Size, Icons } from '@styles/index';
import { InputText, Button } from '@components/global';

const CreateStoreScreen = props => {
    return (
        <View style={styles.screen}>
            <View style={{flex: 1}}>
                <Text allowFontScaling={false} style={styles.title}>Buat Nama Toko Anda, No. Telepon</Text>
                <Text allowFontScaling={false} style={styles.subTitle}>Berikan nama toko Anda dan cantumkan kontak dari toko Anda.</Text>
                <InputText 
                    containerStyle={styles.textInputContainer}
                    placeholder="Berikan Nama Toko"
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    placeholder="Tambahkan No. Telepon"
                />
            </View>
            <Button 
                onPress={() => props.navigation.navigate('SellerNavigation')}
                containerStyle={styles.btnContainer}
                title="Lanjutkan"
            />
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: '',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.replace('MainNavigation')}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    title: {
        color: '#333',
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(26),
        paddingHorizontal: Size.scaleSize(18)
    },
    subTitle: {
        fontFamily: Fonts.mulishRegular,
        paddingHorizontal: Size.scaleSize(18),
        marginTop: Size.scaleSize(8),
        marginBottom: Size.scaleSize(8),
        fontSize: Size.scaleFont(14),
        color: '#B4B6B8'
    },
    textInputContainer: {
        marginHorizontal: Size.scaleSize(18),
        marginTop: Size.scaleSize(23)
    },
    btnContainer: {
        marginHorizontal: Size.scaleSize(18),
        marginBottom: Size.scaleSize(10)
    }
})

export default CreateStoreScreen;