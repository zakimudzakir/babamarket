import React, { useState } from 'react';
import { View, Text, StatusBar, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { useDispatch } from 'react-redux';

import { Icons, Size, Fonts, Colors } from '@styles/index';
import { InputText, Button } from '@components/global';
import { register, updateModal } from '@store/actions';

const RegisterScreen = props => {

    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [securePassword, setSecurePassword] = useState(true);

    const dispatch = useDispatch();

    const onRegister = async () => {
        try{
            await dispatch(register(name, phone, email, password));
            props.navigation.replace('MainNavigation')
        }catch(error){
            dispatch(updateModal(true, 'REGISTER ERROR', error.message))
        }
    }

    return (
        <ScrollView style={{backgroundColor: 'white'}}>
            <View style={styles.screen}>
                <StatusBar 
                    backgroundColor="#FFF"
                    barStyle="dark-content"
                />
                <Text 
                    allowFontScaling={false} 
                    style={styles.titleWelcome}
                >
                    Daftarkan Akun Anda dan Nikmati Berbelanja Disini
                </Text>
                <Text allowFontScaling={false} style={styles.title}>Silahkan lengkapi data Anda</Text>
                <InputText 
                    containerStyle={styles.textInputContainer}
                    leftIcon={<Icons.UserSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    placeholder="Siapa nama lengkap Anda?"
                    label="Nama Lengkap"
                    value={name}
                    onChangeText={setName}
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    keyboardType="phone-pad"
                    leftIcon={<Icons.PhoneSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    placeholder="Masukkan nomor aktif"
                    label="No. Telepon"
                    value={phone}
                    onChangeText={setPhone}
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    keyboardType="email-address"
                    leftIcon={<Icons.EmailSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    placeholder="Masukkan email"
                    label="Email"
                    value={email}
                    onChangeText={setEmail}
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    leftIcon={<Icons.LockedSVG 
                        color={Colors.secondary}
                        size={Size.scaleSize(20)}
                        style={{marginRight: Size.scaleSize(10)}}
                    />}
                    isPassword
                    placeholder="Masukkan kata sandi"
                    label="Kata Sandi"
                    secureTextEntry={securePassword}
                    setSecureTextEntry={setSecurePassword}
                    value={password}
                    onChangeText={setPassword}
                />
                <Button 
                    containerStyle={{marginHorizontal: Size.scaleSize(20), marginTop: Size.scaleSize(40)}}
                    title="Daftar"
                    onPress={onRegister}
                />
                <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 50, alignItems: 'center'}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular}}>Saya sudah memiliki akun? </Text>
                    <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Login')}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: Colors.primary}}>Masuk</Text>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    titleWelcome: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#333',
        fontSize: Size.scaleFont(24),
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(30),
        marginBottom: Size.scaleSize(16)
    },
    title: {
        fontFamily: Fonts.mulishRegular,
        color: '#B4B6B8',
        fontSize: Size.scaleFont(14),
        marginHorizontal: Size.scaleSize(20),
        
    },
    textInputContainer: {
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(23)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Daftarkan Akun',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default RegisterScreen;