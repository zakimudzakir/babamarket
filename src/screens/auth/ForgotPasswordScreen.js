import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

import { Icons, Size, Fonts, Images, Colors } from '@styles/index';
import { Button, InputText } from '@components/global';

const ForgotPasswordScreen = props => {
    return (
        <View style={styles.screen}>
            <View style={styles.content}>
                <Image  
                    style={styles.box}
                    source={Images.BabaMarket}
                    tintColor={Colors.primary}
                />
                <Text allowFontScaling={false} style={styles.label}>Masukkan email anda yang terdaftar untuk mereset kata sandi</Text>
                <InputText 
                    label="Email Aktif Anda"
                    placeholder="Masukkan Email Aktif Anda"
                />
            </View>
            <Button 
                containerStyle={styles.buttonSend}
                title="Kirim"
                onPress={() => props.navigation.navigate('VerificationCode')}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        
    },
    content: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        width: '90%'
    },
    box: {
        width: Size.scaleSize(100), 
        height: Size.scaleSize(100),
        // backgroundColor: '#C4C4C4',
        marginTop: Size.scaleSize(20),
        marginBottom: Size.scaleSize(50),
        
    },
    label: {
        textAlign: 'center',
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(20),
        width: '80%',
        marginBottom: Size.scaleSize(30)
    },
    buttonSend: {
        alignSelf: 'center',
        marginBottom: Size.scaleSize(20), 
        width: '90%',
        marginTop: Size.scaleSize(20)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Lupa Kata Sandi',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default ForgotPasswordScreen;