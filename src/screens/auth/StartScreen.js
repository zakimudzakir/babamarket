import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import { Colors, Size, Fonts, Images } from '@styles/index';
import { Button } from '@components/global';

const StartScreen = props => {
    
    return (
        <View style={styles.screen}>
            <View style={{flex: 1}}>
                <Image 
                    style={styles.box}
                    source={Images.BabaMarket}
                />
            </View>
            <View style={styles.footer}>
                <Text allowFontScaling={false} style={styles.title}>Selamat Datang {"\n"}di Baba Market</Text>
                <Text allowFontScaling={false} style={styles.description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse rhoncus tincidunt penatibus nisi dictum orci.</Text>
                <Button 
                    containerStyle={{backgroundColor: '#FFF'}}
                    textStyle={{color: Colors.primary}}
                    pressColor="white"
                    title="Mulai"
                    onPress={() => props.navigation.navigate('Login')}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: Colors.primary,
        alignItems: 'center',
    },
    box:{
        width: Size.scaleSize(100),
        height: Size.scaleSize(100),
        marginTop: Size.scaleSize(100)
    },
    footer: {
        alignSelf: 'flex-start',
        marginHorizontal: Size.scaleSize(18),
        marginBottom: Size.scaleSize(24)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#FFF',
        fontSize: Size.scaleFont(24)
    },
    description: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        color: '#FFF',
        marginVertical: Size.scaleSize(18)
    }
})

export default StartScreen;