import React, { useEffect } from 'react';
import { View, StyleSheet, Image } from 'react-native';

import { Images, Colors } from '@styles/index';
import { useSelector } from 'react-redux';

const SplashScreen = props => {

    const token = useSelector(state => state.auth.token);

    useEffect(() => {
        didTryLogin();
    }, [])

    const didTryLogin = async () => {
        try{
            setTimeout(() => {
                if(!token){
                    props.navigation.replace('AuthNavigation');
                }else{
                    props.navigation.replace('MainNavigation');
                }
            }, 3000)
        }catch(error){

        }
    }

    return (
        <View style={styles.screen}>
            <Image 
                style={styles.image}
                source={Images.BabaMarket}
                tintColor={Colors.primary}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 200,
        height: 200
    }
})

export default SplashScreen;