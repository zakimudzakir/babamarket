import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';

import { Icons, Fonts, Colors, Size } from '@styles/index';
import { animationHorizontal } from './animation';
import HomeScreen, { screenOptions as homeOptions } from '@screens/main/HomeScreen';
import OrderScreen, { screenOptions as orderOptions } from '@screens/main/OrderScreen';
import ChatScreen, { screenOptions as chatOptions } from '@screens/main/ChatScreen';
import UserScreen, { screenOptions as userOptions } from '@screens/main/UserScreen';
import CategoryScreen, { screenOptions as categoryOptions } from '@screens/main/CategoryScreen';
import ChatDetailScreen, { screenOptions as chatDetailOptions } from '@screens/main/ChatDetailScreen';
import ProductScreen, { screenOptions as productOptions } from '@screens/main/ProductScreen';
// import TestScreen from '@screens/main/TestScreen';
import ProfileScreen, { screenOptions as profileOptions } from '@screens/main/ProfileScreen';
import AddressScreen, { screenOptions as addressOptions } from '@screens/main/AddressScreen';
import StoreScreen, { screenOptions as storeOptions } from '@screens/main/StoreScreen';
import CartScreen, { screenOptions as cartOptions } from '@screens/main/CartScreen';

const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

const TabNavigation = props => {
    return (
        <BottomTab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.primary,
                inactiveTintColor: '#CDCFD0',
                labelStyle: {
                    fontFamily: Fonts.mulishRegular,
                    fontSize: Size.scaleFont(12)
                },
                allowFontScaling: false,
                style: {
                    height: Size.scaleSize(50),
                    paddingTop: Size.scaleSize(5)
                },
                keyboardHidesTabBar: true,
                tabBarHideOnKeyboard: true
            }}
            
        >
            <BottomTab.Screen 
                name="HomeTab"
                component={HomeStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Home";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Beranda",
                        tabBarIcon: props => (
                            <Icons.HomeSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="OrderTab"
                component={OrderStack}
                options={({route}) => {
                    return {
                        title: "Pesanan",
                        tabBarIcon: props => (
                            <Icons.OrderSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="ChatTab"
                component={ChatStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Chat";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Obrolan",
                        tabBarIcon: props => (
                            <Icons.ChatSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="UserTab"
                component={UserStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "User";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Akun",
                        tabBarIcon: props => (
                            <Icons.UserCircleSVG
                                size={24}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
        </BottomTab.Navigator>
    )
}

const HomeStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Home"
                component={HomeScreen}
                options={homeOptions}
            />
            <Stack.Screen 
                name="Category"
                component={CategoryScreen}
                options={categoryOptions}
            />
            <Stack.Screen 
                name="Product"
                component={ProductScreen}
                options={productOptions}
            />
            <Stack.Screen 
                name="Store"
                component={StoreScreen}
                options={storeOptions}
            />
            <Stack.Screen 
                name="Cart"
                component={CartScreen}
                options={cartOptions}
            />
            <Stack.Screen 
                name="Chat"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
        </Stack.Navigator>
    )
}

const OrderStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
        }}>
            <Stack.Screen 
                name="Order"
                component={OrderScreen}
                options={orderOptions}
            />
        </Stack.Navigator>
    )
}

const ChatStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Chat"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
        </Stack.Navigator>
    )
}

const UserStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="User"
                component={UserScreen}
                options={userOptions}
            />
            <Stack.Screen 
                name="Profile"
                component={ProfileScreen}
                options={profileOptions}
            />
            <Stack.Screen 
                name="Address"
                component={AddressScreen}
                options={addressOptions}
            />
        </Stack.Navigator>
    )
}

export default TabNavigation;