import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { animationHorizontal } from './animation';
import { Size, Fonts } from '@styles/index';
import StartScreen from '@screens/auth/StartScreen';
import LoginScreen, { screenOptions as loginOptions } from '@screens/auth/LoginScreen';
import RegisterScreen, { screenOptions as registerOptions } from '@screens/auth/RegisterScreen';
import ForgotPasswordScreen, { screenOptions as forgotOptions } from '@screens/auth/ForgotPasswordScreen';
import VerificationCodeScreen, { screenOptions as verificationOptions } from '@screens/auth/VerificationCodeScreen';
import ResetPasswordScreen, { screenOptions as resetOptions } from '@screens/auth/ResetPasswordScreen';

const Stack = createStackNavigator();

const AuthNavigation = props => {
    return (
        <Stack.Navigator 
            screenOptions={{ 
                ...animationHorizontal,
                headerTitleAlign: 'left',
                headerTitleStyle: {
                    fontFamily: Fonts.mulishExtraBold,
                    fontSize: Size.scaleFont(16),
                    color: '#333333'
                }
            }}
        >
            <Stack.Screen 
                name="Start"
                component={StartScreen}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name="Login"
                component={LoginScreen}
                options={loginOptions}
            />
            <Stack.Screen 
                name="Register"
                component={RegisterScreen}
                options={registerOptions}
            />
            <Stack.Screen 
                name="ForgotPassword"
                component={ForgotPasswordScreen}
                options={forgotOptions}
            />
            <Stack.Screen 
                name="VerificationCode"
                component={VerificationCodeScreen}
                options={verificationOptions}
            />
            <Stack.Screen 
                name="ResetPassword"
                component={ResetPasswordScreen}
                options={resetOptions}
            />
        </Stack.Navigator>
    )
}

export default AuthNavigation;