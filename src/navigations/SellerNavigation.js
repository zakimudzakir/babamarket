import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';

import { animationHorizontal } from './animation';
import { Colors, Fonts, Size, Icons } from '@styles/index';
import DashboardScreen, { screenOptions as dashboardOptions } from '@screens/main/seller/DashboardScreen';
import TarikDanaScreen, { screenOptions as tarikDanaOptions } from '@screens/main/seller/TarikDanaScreen';
import DetailTarikDanaScreen, { screenOptions as detailTarikDanaOptions } from '@screens/main/seller/DetailTarikDanaScreen';
import SellingScreen, { screenOptions as sellingOptions } from '@screens/main/seller/SellingScreen';
import MyProductListScreen, { screenOptions as myProductListOptions } from '@screens/main/seller/MyProductListScreen';
import ChatScreen, { screenOptions as chatOptions } from '@screens/main/ChatScreen';
import ChatDetailScreen, { screenOptions as chatDetailOptions } from '@screens/main/ChatDetailScreen';
import SellerScreen, { screenOptions as sellerOptions } from '@screens/main/seller/SellerScreen';
import ProfileScreen, { screenOptions as profileOptions } from '@screens/main/ProfileScreen';
import InputProductScreen, { screenOptions as inputProductOptions } from '@screens/main/seller/InputProductScreen';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

const TabNavigation = props => {
    return (
        <BottomTab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.primary,
                inactiveTintColor: '#CDCFD0',
                labelStyle: {
                    fontFamily: Fonts.mulishRegular,
                    fontSize: Size.scaleFont(12)
                },
                allowFontScaling: false,
                style: {
                    height: Size.scaleSize(50),
                    paddingTop: Size.scaleSize(5)
                },
                keyboardHidesTabBar: true,
                tabBarHideOnKeyboard: true
            }}
            
        >
            <BottomTab.Screen 
                name="DashboardSellerTab"
                component={HomeStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "DashboardSeller";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Dashboard",
                        tabBarIcon: props => (
                            <Icons.HomeSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="SellingTab"
                component={SellingStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Selling";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Penjualan",
                        tabBarIcon: props => (
                            <Icons.ClipboardNotesSVG
                                size={28}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="MyProductTab"
                component={ProductStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "MyProductList";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Produk Saya",
                        tabBarIcon: props => (
                            <Icons.BoxSVG
                                size={30}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="SellerTab"
                component={SellerStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Seller";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Pengaturan",
                        tabBarIcon: props => (
                            <Icons.StoreSVG
                                size={24}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
        </BottomTab.Navigator>
    )
}

const HomeStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            
            <Stack.Screen 
                name="DashboardSeller"
                component={DashboardScreen}
                options={dashboardOptions}
            />
            <Stack.Screen 
                name="TarikDana"
                component={TarikDanaScreen}
                options={tarikDanaOptions}
            />
            <Stack.Screen 
                name="DetailTarikDana"
                component={DetailTarikDanaScreen}
                options={detailTarikDanaOptions}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatSellerDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
        </Stack.Navigator>
    )
}

const SellingStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            
            <Stack.Screen 
                name="Selling"
                component={SellingScreen}
                options={sellingOptions}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatSellerDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
        </Stack.Navigator>
    )
}

const ProductStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="MyProductList"
                component={MyProductListScreen}
                options={myProductListOptions}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatSellerDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
            <Stack.Screen 
                name="InputProduct"
                component={InputProductScreen}
                options={inputProductOptions}
            />
        </Stack.Navigator>
    )
}

const SellerStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Seller"
                component={SellerScreen}
                options={sellerOptions}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatSellerDetail"
                component={ChatDetailScreen}
                options={chatDetailOptions}
            />
            <Stack.Screen 
                name="Profile"
                component={ProfileScreen}
                options={profileOptions}
            />
        </Stack.Navigator>
    )
}


export default TabNavigation;