import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { animationHorizontal } from './animation';
import { Size, Fonts } from '@styles/index';
import CreateStoreScreen, { screenOptions as createStoreOptions } from '@screens/auth/seller/CreateStoreScreen';

const Stack = createStackNavigator();

const AuthSellerNavigation = props => {
    return (
        <Stack.Navigator 
            screenOptions={{ 
                ...animationHorizontal,
                headerTitleAlign: 'left',
                headerTitleStyle: {
                    fontFamily: Fonts.mulishExtraBold,
                    fontSize: Size.scaleFont(16),
                    color: '#333333'
                }
            }}
        >
            <Stack.Screen 
                name="CreateStore"
                component={CreateStoreScreen}
                options={createStoreOptions}
            />
            
        </Stack.Navigator>
    )
}

export default AuthSellerNavigation;