import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { useSelector, useDispatch } from 'react-redux';

import SplashScreen from '@screens/startup/SplashScreen';
import AuthNavigation from './AuthNavigation';
import MainNavigation from './MainNavigation';
import SellerNavigation from './SellerNavigation';
import AuthSellerNavigation from './AuthSellerNavigation';
import { Alert, Loader } from '@components/global';
import { showModal } from '@store/actions';

const Stack = createStackNavigator();

const RootNavigation = props => {
    // const isAuth = useSelector(state => !!state.auth.token);
    // const didTryAutoLogin = useSelector(state => state.ui.didTryAutoLogin);

    const modal = useSelector(state => state.ui.modal);
    const loading = useSelector(state => state.ui.loading);
    const dispatch = useDispatch();

    return (
        <SafeAreaProvider>
            <Alert 
                show={modal.show}
                title={modal.title}
                message={modal.message}
                modalClosed={() => dispatch(showModal(false))}
            />
            <Loader show={loading} />
            <NavigationContainer>
                <Stack.Navigator screenOptions={{headerShown: false}}>
                    <Stack.Screen name="Splash" component={SplashScreen} />
                    <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
                    <Stack.Screen name="MainNavigation" component={MainNavigation} />
                    <Stack.Screen name="AuthSellerNavigation" component={AuthSellerNavigation} />
                    <Stack.Screen name="SellerNavigation" component={SellerNavigation} />
                </Stack.Navigator>
            </NavigationContainer>
        </SafeAreaProvider>
    )
}

export default RootNavigation;